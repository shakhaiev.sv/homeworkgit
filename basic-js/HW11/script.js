'use strict'

//Технічні вимоги:
// У файлі index.html лежить розмітка двох полів вводу пароля.
// Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд.У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
// Коли пароля не видно - іконка поля має виглядати як та, що в першому полі(Ввести пароль)
// Коли натиснута іконка, вона має виглядати, як та, що у другому полі(Ввести пароль)
// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// Якщо значення збігаються – вивести модальне вікно(можна alert) з текстом – You are welcome;
// Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
// Після натискання на кнопку сторінка не повинна перезавантажуватись
// Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.

let form = document.querySelector('.password-form')
let verifInput = form.querySelectorAll('input')[1]

verifInput.addEventListener('blur', function () {
    let input = document.querySelectorAll('.password-form input');
    let btn = form.querySelector('.btn')
    if (String(input[0].value) !== String(input[1].value)) {
        let errTextPassword = document.createElement('p');
        errTextPassword.className = 'invalid'
        errTextPassword.textContent = 'Потрібно ввести однакові значення'
        errTextPassword.style.color = 'red';
        btn.before(errTextPassword)
    }
})

verifInput.addEventListener('focus', function () {
    let p = form.querySelector('.invalid')
    if (p) { p.remove() }
})

form.addEventListener('click', function (event) {
    let target = event.target;
    if (target.closest('.password-form')) {
        if (target.classList.contains('fas') && target.classList.contains('icon-password')) {
            if (target.classList.contains('fa-eye')) {
                target.classList.replace('fa-eye', 'fa-eye-slash')
                target.closest('label').querySelector('input').type = 'text'
            } else {
                target.classList.replace('fa-eye-slash', 'fa-eye')
                target.closest('label').querySelector('input').type = 'password'
            }
        }
    }
})


form.addEventListener('submit', onSubmit)
function onSubmit(event) {
    let input = document.querySelectorAll('.password-form input');
    if (input[0].value === '' || input[1].value === '') return;
    if (String(input[0].value) === String(input[1].value)) { alert('You are welcome') };
    event.preventDefault();
}

