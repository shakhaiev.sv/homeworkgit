'use strict'

// При завантаженні сторінки – показати на ній кнопку з текстом "Намалювати коло". Дана кнопка повинна бути єдиним контентом у тілі HTML документа, решта контенту потрібно створити і додати на сторінку за допомогою Javascript.
// При натисканні на кнопку "Намалювати коло" показувати одне поле введення - діаметр кола. При натисканні на кнопку "Намалювати" створити на сторінці 100 кіл (10*10) випадкового кольору. При кліку на конкретне коло - це коло має зникати, у своїй порожнє місце заповнюватися, тобто інші кола зрушуються вліво.
// У вас може виникнути бажання поставити обробник події на кожне коло для його зникнення. Це неефективно, так не треба робити. На всю сторінку має бути лише один обробник подій, який це робитиме.


let btnPaintCircle = document.createElement('button');
btnPaintCircle.textContent = 'Намалювати коло';
btnPaintCircle.className = 'paint-circle'
btnPaintCircle.style.cssText = 'display: block; margin: 50px auto;  padding: 10px;'
document.body.append(btnPaintCircle)

btnPaintCircle.addEventListener('click', function () {
    let diametercircle = document.querySelector('.input-lenght-circle');
    let lengthCircle = document.createElement('input');
    let paintCircleDiam = document.createElement('button')

    if (!diametercircle) {
        lengthCircle.setAttribute('type', 'text');
        lengthCircle.setAttribute('placeholder', 'Enter the diameter of circles (size: cm)');
        lengthCircle.style.cssText = 'display: block; margin: 20px auto;  padding: 10px; width:300px;'
        lengthCircle.className = 'input-lenght-circle'

        paintCircleDiam.textContent = 'Намалювати';
        paintCircleDiam.className = 'paint-circle-diameter'
        paintCircleDiam.style.cssText = 'display: block; margin: 10px auto;  padding: 10px ; background-color: green; border: none; border-radius: 5px'

        btnPaintCircle.after(lengthCircle)
        lengthCircle.after(paintCircleDiam)

        diametercircle = document.querySelector('.input-lenght-circle');

    }

    diametercircle.addEventListener('blur', function () {
        if (diametercircle.value === '' || isNaN(diametercircle.value)) {
            diametercircle.value = ''
            diametercircle.setAttribute('placeholder', 'Enter the diameter of circles (size: cm) . ARIGHT!');
            diametercircle.style.cssText = 'display: block; margin: 20px auto;  padding: 10px; width:300px; border: 2px solid  red'
            diametercircle.className = 'input-lenght-circle'
        } else {
            diametercircle.style.cssText = 'display: block; margin: 20px auto;  padding: 10px; width:250px;'
        }
    })

    paintCircleDiam.addEventListener('click', function () {
        if (diametercircle.value === '' || isNaN(diametercircle.value)) return;
        console.log(diametercircle.value);
        let ul = document.createElement('ul')
        ul.className = 'list-circle'
        ul.style.cssText = 'list-style-type: none; display: grid; margin-top: 25px;  padding: 0; gap: 2px;  grid-template-columns: repeat(10,1fr); width: 95vw;justify-items: center; margin:20px  auto;'
        for (let i = 0; i < 100; i++) {
            let li = document.createElement('li')
            li.className = 'circle-item'
            li.style.cssText = `background-color: ${randomColor()}; width: ${diametercircle.value}cm; height:${diametercircle.value}cm ;border-radius: 50%`
            ul.append(li)
        }
        paintCircleDiam.after(ul)

        ul.addEventListener('click', function (event) {
            console.log(event.target);
            if (!event.target.closest('.list-circle') || event.target.classList.contains('list-circle')) return;
            event.target.remove()
        })
    }, { once: true })
})

function randomColor() {
    let r = Math.floor(Math.random() * 255);
    let g = Math.floor(Math.random() * 255);
    let b = Math.floor(Math.random() * 255);

    return `rgb(${r},${g},${b})`
}

