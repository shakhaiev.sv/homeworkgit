'use strict'


//Реализовать функцию полного клонирования объекта.
//Задача должна быть реализована на языке javascript, без
//использования фреймворков и сторонник библиотек(типа Jquery).
//Технические требования:
//Написать функцию для рекурсивного полного клонирования объекта
// (без единой передачи по ссылке, внутренняя вложенность свойств
//объекта может быть достаточно большой).
//Функция должна успешно копировать свойства в виде объектов
//и массивов на любом уровне вложенности.
//В коде нельзя использовать встроенные механизмы клонирования,
//такие как функция Object.assign() или спред-оператор.

let exmapleObj = {
    property1: 'value1',
    property2: {
        property2_1: 'value2_1',
        property2_2: {
            property2_2_1: 'value2_2_1',
            property2_2_2: 'value2_2_2',
            property2_2_3: {
                property2_2_3_1: 'value2_2_3_1',
                property2_2_3_2: 'value2_2_3_2',
            }
        }
    },
    property3: 'value3',
    property4: null,
    propertyArr1: [1, 2, 3, 4, 5],
    propertyArr2: [1, {
        propertyArr2_1: 'value1_1',
        propertyArr2_1: [1, 2, 3, {
            propertyArr2_1_1: 'value1_1_1',
            propertyArr2_1_2: 'value1_1_2'
        }]
    }, 3, 4, 5]

}


function cloneObject(obj) {
    let newObject = {}
    for (let key in obj) {
        if (typeof obj[key] === 'object' && obj[key] !== null) {
            cloneObject(obj[key])
        }
        newObject[key] = obj[key];
    }
    return newObject
}
console.log(exmapleObj);


let newObj = cloneObject(exmapleObj)
console.log(newObj);



