'use strict'

//Опишіть, як можна створити новий HTML тег на сторінці.
// створюємо тег  за допомогою  -let tag =   document.createElement('tag')
// додаємо тег на сторінку -  document.body.prepend(tag)

//Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// node.insertAdjacentHTML(par1, par 2 )
// par1 відображає як ми маємо вставити тег beforebegin/afterbegin/beforend/afterend
// par2 можна вказати любий тег приклад запи '<p>tag</p>

//Як можна видалити елемент зі сторінки?
// метод  element.remove()


createList(["Kharkiv", "Kiev", ["Borispol", "Irpin", ['Bucha', 'Hostomel', 'Romanivka']], "Odessa", "Lviv", "Dnieper"]);

function createList(arr, parent = document.body) {
    let ul = document.createElement('ul');
    function controlArray(arr) {
        let nowAr = arr.map((elem) => {
            if (Array.isArray(elem)) {
                let ul = document.createElement('ul');
                ul.append(...controlArray(elem));
                return ul;
            } else {
                let li = document.createElement('li');
                li.textContent = elem;
                return li;
            };
        });
        return nowAr;
    };
    ul.append(...controlArray(arr));
    parent.append(ul);
}

let timer = 3;
let p = document.createElement('p')
p.textContent = `Сторінка очиститься через ${timer}`
document.body.prepend(p)

let reverseTimer = setInterval(() => {
    timer -= 1;
    p.textContent = `Сторінка очиститься через ${timer}`
}, 1000)

setTimeout(() => {
    clearTimeout(reverseTimer)
    document.body.innerHTML = ''
}, 3000)
