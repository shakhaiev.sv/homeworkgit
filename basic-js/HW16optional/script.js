'use strict'

//Технические требования:
//Написать функцию для подсчета n - го обобщенного числа Фибоначчи.
//Аргументами на вход будут три числа - F0, F1, n, где F0, F1 - первые два числа последовательности(могут быть любыми целыми числами), 
//n - порядковый номер числа Фибоначчи, которое надо найти.Последовательнось будет строиться по следующему правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.
//Считать с помощью модального окна браузера число, которое введет пользователь(n).
//С помощью функции посчитать n - е число в обобщенной последовательности Фибоначчи и вывести его на экран.
//Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу(F - 1 = F - 3 + F - 2).

let numberFibonacciOne, numberFibonacciTwo, numberOrderFibonacci;

let controlPromp = (number) => number === 'null' || number === '' || isNaN(number);

do {
    numberFibonacciOne = +prompt('первое  число последовательности')
}
while (controlPromp(numberFibonacciOne))
do {
    numberFibonacciTwo = +prompt('второе  число последовательности')
}
while (controlPromp(numberFibonacciTwo))
do {
    numberOrderFibonacci = +prompt('порядковый номер числа Фибоначчи')
}
while (controlPromp(numberOrderFibonacci))

function fibonacci(f0, f1, num) {

    if (num === 0) {
        return f0
    } else if (num === 1) {
        return f1
    } else if (num < 0) {
        return fibonacci(f0, f1, num + 2) - fibonacci(f0, f1, num + 1)
    } else {
        return fibonacci(f0, f1, num - 1) + fibonacci(f0, f1, num - 2)
    }
}

alert(fibonacci(numberFibonacciOne, numberFibonacciTwo, numberOrderFibonacci));
