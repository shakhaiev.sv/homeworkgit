

// При натисканні клавіш M+ або M- у лівій частині табло необхідно показати маленьку букву m - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання MRC має очищати пам'ять.


"use strict"
let calculator = document.querySelector('.box')
let numberArr = [...document.querySelectorAll('.black')].filter(e => e.value !== 'C')
let operatorArr = [...document.querySelectorAll('.pink')]
let display = document.querySelector('.display').firstElementChild
let mrcDisplay = false // для видалення із пам'яті
let resultmrc = 0; // другий натиск якщо сума в сховищі не змінилася 
let mrcDb = 0;  // збереження значення в пам'яті
let num1 = ""; // число до знака
let num2 = ''; // число після знака
let operator = ''; // знак
let result = '';  // результат
let string = ""; // вираз
let newCalc = false; // вираз починається із результату 

calculator.addEventListener('click', calc)

function calc(event) {
    let target = event.target
    // перевірка чи є дані числа в масиві 
    let controlEl = (arr) => arr.find(e => e.value === target.value)
    // клік , якщо не кнопка  
    if (!target.closest('.button')) return;

    // після результату дані і очищення змінних 
    if (newCalc) {
        string = `${result}`
        operator = "";
        num2 = ''
        num1 = ''
        newCalc = !newCalc
    }

    // при натисканні m+
    if (target.value === 'm+') {
        mrcDb += Number(display.value)
        mrcDisplay = true
    }

    // при натисканні m-
    if (target.value === 'm-') {
        mrcDb -= Number(display.value)
        mrcDisplay = true
    }

    //  відображення числа в пам'яті
    if (target.value === 'mrc') {
        if (mrcDisplay) {
            display.value = mrcDb
            string = mrcDb
            mrcDisplay = false
        } else {
            display.value = ""
            string = ""
            mrcDb = 0
        }
    }

    // ввод цифр в display

    if (controlEl(numberArr)) {

        if (operator !== "" && num1 !== "" && isNaN(string[string.length - 1])) {
            display.value = ""
            string += target.value
            display.value += target.value
        } else {
            string += target.value
            display.value += target.value
        }
        mrcDisplay = true
    }

    if (controlEl(operatorArr)) {
        switch (true) {
            // не відображати  першого знак всі крім "-"
            // після мінуса проставляти любі знаки 
            // після знака проставляти додатковий знак
            case (string.length === 0 && target.value !== "-" ||
                string[0] === '-' && target.value !== '' && string.length < 2 ||
                operator !== "" && target.value !== '-'): {
                    operator
                    string
                    break
                };

            case (string[0] === '-' && target.value === '-' && operator === ""): {
                //мінусове число і знак мінус
                string += target.value;
                operator = target.value;
                num1 = string[0] + string.slice(1, string.slice(1).indexOf(operator) + 1)
                break
            };

            case (operator !== "" && num1 !== "" && target.value === '-' ||
                operator !== "" && num1 !== "" ||
                string[0] === '-' && operator !== "" && num1 !== "" ||
                string.length === 0 && target.value === '-' ||
                string[0] === '-' && operator !== "" && num1 !== ""): {
                    operator;
                    string += target.value;
                    break
                };

            default: {
                string += target.value
                operator = target.value
                num1 = string.slice(0, string.indexOf(operator))
            }
        }
        mrcDisplay = true

    }

    // C 
    if (target.value === 'C') {
        display.value = ''
        string = ''
        operator = "";
        num1 = "";
        num2 = "";
        result = ""
    }

    // = 
    if (target.closest('.orange')) {
        if (string === "" || operator === "") return;
        // якщо мінусові значення 
        if (string[0] === '-' && operator === '-') {
            num2 = string.slice(string.slice(1).indexOf(operator) + 2)
        } else {
            num2 = string.slice(string.indexOf(operator) + 1);
        }

        result = calcNumber(operator, num1, num2)
        display.value = result
        newCalc = !newCalc
    }
}



function calcNumber(operator, num1, num2) {

    num1 = Number(num1)
    num2 = Number(num2)

    switch (true) {
        case (operator === '+'): {
            return num1 + num2
            break
        }
        case (operator === '-'): {
            return num1 - num2
            break
        }
        case (operator === '/'): {
            if (num2 === 0) return 0
            return num1 / num2
            break
        }
        case (operator === '*'): {
            return (num1 * num2)
            break
        }
    }

}

