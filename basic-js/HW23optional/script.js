// Опишіть своїми словами, що таке обробник подій.
//
// Опишіть, як додати обробник подій до елемента. Який спосіб найкращий і чому?


// Завдання

// Технічні вимоги:
// При завантаженні сторінки показати користувачеві поле вводу (input) з написом Price. Це поле буде служити для введення числових значень
// Поведінка поля має бути наступною:
// При фокусі на полі вводу - має з'явитися рамка зеленого кольору. У разі втрати фокусу вона пропадає.
// Коли прибрано фокус з поля - його значення зчитується, над полем створюється span, у якому має бути текст: Поточна ціна: ${значення з поля вводу}. Поруч із ним має бути кнопка з хрестиком (X). Значення всередині поля вводу забарвлюється у зелений колір.
// При натисканні на Х - span з текстом та кнопка X повинні бути видалені. Значення, введене у поле вводу, обнулюється.
// Якщо користувач ввів число менше 0 – при втраті фокусу підсвічувати поле введення червоною рамкою, під полем виводити фразу – Please enter correct price. span з некорректним значенням при цьому не створюється.
// У папці img лежать приклади реалізації поля вводу та "span", які можна брати як приклад




let form = document.createElement('form');
form.setAttribute('id', 'form')
form.style.cssText = 'margin : 10px auto'

let labelPrice = document.createElement('label')
labelPrice.innerText = 'Price';
labelPrice.setAttribute('for', 'price');
labelPrice.style.cssText = 'font-size: 18px'
let inputPrice = document.createElement('input');
inputPrice.setAttribute('type', 'text');
inputPrice.setAttribute('id', 'price');
inputPrice.setAttribute('placeholder', 'Enter number');
inputPrice.style.cssText = 'margin-left: 5px; padding: 5px;'

document.body.prepend(form);
form.append(labelPrice, inputPrice)


inputPrice.addEventListener('focus', function () {
    let spanError = document.querySelector('.error')
    inputPrice.value = ''
    if (spanError) {
        spanError.remove()
    }
    inputPrice.style.cssText = 'margin-left: 5px; padding: 5px ; outline-color: green';


})

inputPrice.addEventListener('blur', function () {
    inputPrice.style.cssText = 'margin-left: 5px; padding: 5px;';
    if (inputPrice.value === '' || isNaN(inputPrice.value) || +inputPrice.value <= 0) {
        let spanError = document.createElement('span')
        let br = document.createElement('br')
        spanError.className = 'error'
        spanError.innerText = 'Please enter correct price.'
        inputPrice.after(br, spanError)

    } else {
        inputPrice.style.cssText = 'margin-left: 5px; padding: 5px; color:green;';
        let div = document.createElement('div');
        div.style.cssText = 'display: flex; gap: 5px; margin-top: 5px'

        let span = document.createElement('span');
        span.innerText = `Поточна ціна: ${inputPrice.value}`
        let btnX = document.createElement('button')
        btnX.innerText = 'X'

        div.append(span, btnX)
        form.append(div)
    }
})

form.addEventListener('click', function (event) {
    let btnX = event.target.closest('button')
    if (!btnX) return;
    btnX.closest('div').remove()
})



