'use strict'

let tabsList = document.querySelector('.tabs');



tabsList.addEventListener('click', ClickMenuContent);

function ClickMenuContent(event) {
    let liTabsTitle = tabsList.querySelectorAll('li');
    let liTabsContent = document.querySelectorAll('.tabs-content li');

    [...liTabsTitle].map((element) => (event.target.dataset.title === element.dataset.title) ? element.classList.add('active') : element.classList.remove('active'));

    [...liTabsContent].map((element) => (event.target.dataset.title === element.dataset.text) ? element.classList.remove('сontent-del') : element.classList.add('сontent-del'));

}

