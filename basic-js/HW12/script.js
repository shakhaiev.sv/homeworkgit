'use strict'

// Теоретичні питання

// Чому для роботи з input не рекомендується використовувати клавіатуру?
// Відповідь 

// Завдання
// Реалізувати функцію підсвічування клавіш. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:

// У файлі index.html лежить розмітка для кнопок.

// Кожна кнопка містить назву клавіші на клавіатурі
// Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.




document.addEventListener('keydown', function (event) {
    let btn = document.querySelector('.btn-wrapper').children;
    let isNumberBtn = [...btn].findIndex((element) => element.textContent.toLowerCase() === event.key.toLowerCase());
    if (isNumberBtn === -1) return;
    [...btn].forEach(element => element.style.cssText = 'background-color: #000000;');
    [...btn][isNumberBtn].style.cssText = 'background-color: blue'
})




