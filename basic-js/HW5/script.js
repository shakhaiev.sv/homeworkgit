'use strict'

///Опишіть своїми словами, що таке метод об'єкту
//відповідь: вбудовані властивості об'єкта, якими являються функції , які працюються зі змінними всередині об'єкта. 
//Який тип даних може мати значення властивості об'єкта?
//відповідь: string, number , boolean , object , array
//Об'єкт це посилальний тип даних. Що означає це поняття?
//відповідь: тобто об'єкти не зберігаються повністю у змінну, а змінна збергіє посиланням на ячейку пам'яті де зберігається об'єкт 


//version  work
const createNewUser = function () {

    const newUser = {
        getLogin() {
            return (`${this.firstName.charAt(0)}${this.lastName}`).toLowerCase();
        },
        setFirstName(value) {
            Object.defineProperty(this, 'firstName', {
                value: value
            })
        },
        setLastName(value) {
            Object.defineProperty(this, 'lastName', {
                value: value
            })
        }
    }
    Object.defineProperties(newUser, {
        'firstName': {
            value: prompt('Enter name'),
            writable: false,
            configurable: true
        },
        'lastName': {
            value: prompt('Enter surname'),
            writable: false,
            configurable: true
        }
    });
    return newUser
}


let user = createNewUser();
console.log(user);
console.log(user.getLogin());


user.setFirstName('IVAN')
console.log(user);






//Variant start (ERROR Cannot assign to read only property 'firstName' of object '#<Object>')
/* const createNewUser = function () {

    const newUser = {
        getLogin() {
            return (`${this.firstName.charAt(0)}${this.lastName}`).toLowerCase();
        },
        setFirstName(value) {
            this.firstName = value
        },
        setLastName(value) {
            this.lastName = value
        }
    }
    Object.defineProperties(newUser, {
        'firstName': {
            value: prompt('Enter name'),
            writable: false,
            configurable: true
        },
        'lastName': {
            value: prompt('Enter surname'),
            writable: false,
            configurable: true
        }
    });
    return newUser
}


let user = createNewUser()

console.log(user);
console.log(user.getLogin());


user.setFirstName('IVAN')
console.log(user);
 */


