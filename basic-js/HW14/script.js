'use strict'
// Завдання
// Реалізувати можливість зміни колірної теми користувача.Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:

// Взяти будь-яке готове домашнє завдання з HTML/CSS.
// Додати на макеті кнопку "Змінити тему".
// При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
// Вибрана тема повинна зберігатися після перезавантаження сторінки

let BtnChangeTopic = document.createElement('button')
BtnChangeTopic.textContent = 'Змінити тему'
BtnChangeTopic.classList.add('btn-change-topic')
document.body.prepend(BtnChangeTopic)


window.addEventListener('load', function () {
    let bgBodyLoad = document.querySelector('.page-body')
    let isClassBG = + localStorage.bgcBody
    if (isClassBG) {
        bgBodyLoad.classList.add('change-topic')
    } else {
        bgBodyLoad.classList.remove('change-topic')
    }

})

BtnChangeTopic.addEventListener('click', function () {
    let bgBody = document.querySelector('.page-body')
    if (bgBody.classList.contains('change-topic')) {
        bgBody.classList.remove('change-topic')
        localStorage.setItem('bgcBody', 0)
    } else {
        bgBody.classList.add('change-topic')
        localStorage.setItem('bgcBody', 1)
    }
})