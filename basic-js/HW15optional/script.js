'use strict'

let numberUser;
do {
    numberUser = prompt('enter number', numberUser)
}
while (numberUser === 'null' || numberUser === '' || isNaN(numberUser))
function factorial(num) {
    if (num === 0) {
        return 1
    } else {
        return num * factorial(num - 1)
    }
}
alert(`${factorial(numberUser)}`);   