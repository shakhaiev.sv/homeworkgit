
function filterCollection(array, filtName, booleanChek, ...arg) {
    let filterSearh = []
    let filtNameArray = filtName.split(' ')
    // створюю новий масив, елементи через точку формую в масив
    let arrayArgum = arg.map(element => (element.includes('.')) ? element.split('.') : element)

    function multiObject(obj, prop) {
        // obj = елемент array / prop = filtName із точкою і перебираємо 
        for (let property of prop) {
            // перевірка на відсутність і перевірска вмістє обєктів
            if (obj[property] === undefined || (prop.length > 1 && typeof obj[property] !== 'object')) return false;
            if (typeof obj[property] === 'object') {
                let valid = multiObject(obj[property], prop.filter(element => element !== property))
                // якщо вірне то відправляємо 
                if (valid) {
                    return ({ [property]: valid });
                }
                return false
            } else {
                //перевірка останнього значення  на умову filtName
                for (let key of filtNameArray) {
                    if (obj[property].toUpperCase() === key.toUpperCase()) {
                        return { [property]: obj[property] }

                    }
                }
            }
        }
    }

    arrayArgum.forEach((element) => {
        // перевірка на booleanChek
        if (!booleanChek && filterSearh.length >= 1) {
            return
        }
        //перевірка на масив 
        if (Array.isArray(element)) {
            for (let i = 0; i < array.length; i++) {
                // перевірка першого знач масива і чи являється він обєктом 
                if (array[i][element[0]] === undefined
                    || typeof array[i][element[0]] !== 'object') continue;
                // так виклик функції
                let valid = (multiObject(array[i], element))
                if (valid) {
                    filterSearh.push(valid);
                }
            }
        } else {
            // якщо елемент не масив 
            for (let i = 0; i < array.length; i++) {
                //перевіряємо значення 
                for (let key of filtNameArray) {
                    //якщо значення відсунє переривамо цикл
                    if (array[i][element] === undefined) continue;
                    if (array[i][element].toUpperCase() === key.toUpperCase()) {
                        //якщо співпадіння додаємо в масив
                        filterSearh.push({ [element]: array[i][element] })
                    }
                }
            }
        }
    })
    return filterSearh
}

// цикл  взяти 4 аргумент і перебирати масив vechiles
// написати додаткову функція на пошук додатково аргумента 

let vehicles = [
    {
        name: 'Toyota',
        car: {
            name: 'en_US',
            model: 'pajero'
        },
    },
    {
        description: 'en_US',
    },
    {
        locales: 'asdasda',
        contentType: {
            name: 'Toyota',
            desc: 'asdasd'
        }
    },

    {
        locales: {
            name: 'Toyota',
            description: 'en_US'
        }

    },
    {
        locales: 'asdasda',
        contentType: {
            name: 'en_US',
            desc: 'asdasd'
        }
    },
    {
        car: {
            name: {
                description: 'Toyota',
                fhgcgf: 'gfgfc'
            },
            desx: 'adsajda'
        }
    },
    {
        locales: 'asdasda',
        contentType: {
            name: 'en_US',
            desc: 'asdasd'
        }
    },
    {
        car: {
            name: {
                descrip: 'Toyota',
                fhgcgf: 'gfgfc',
                property: {
                    desc: 'Toyota'
                }

            },
            desx: 'adsajda'
        }
    },

    {
        car: {
            name: [
                {
                    descrip: 'Toyota',
                    fhgcgf: 'gfgfc',
                    property: {
                        desc: 'Toyota'
                    }

                },
                {
                    info: 'Toyota'
                }
            ],
            desx: 'adsajda'
        }
    },

]

// перевірка на вложеність 
console.log(filterCollection(vehicles, 'en_US Toyota', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description', 'car.name.descrip', 'car.name.property.desc'))
//перевірка на 3 параметр
console.log(filterCollection(vehicles, 'en_US Toyota', false, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description', 'car.name.descrip', 'car.name.property.desc'))
// перевірка на регістр
console.log(filterCollection(vehicles, 'en_us TOYOTA', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description', 'car.name.descrip', 'car.name.property.desc'))
