'use strict'

let btnPrevious = document.querySelector('.page-navbar-team .btn-prev')
let btnNext = document.querySelector('.page-navbar-team .btn-next')
let listPicTeam = [...document.querySelectorAll('.navbar-team .team-item-picture')];
let currentIndex = listPicTeam.findIndex(e => e.classList.contains('active-slid'))
let listTeamInfo = [...document.querySelectorAll('.team-list .team-item')]

btnPrevious.addEventListener('click', showPreviSlide)
btnNext.addEventListener('click', showNextSlide)

function showNextSlide() {
    listTeamInfo.forEach((el, i) => (i === currentIndex) ? el.classList.add('remove-slide-info') : el.classList.remove('remove-slide-info'));
    setTimeout(() => {
        currentIndex = (currentIndex + 1) % listPicTeam.length
        showSlide(currentIndex)
    }, 1000)
}

function showPreviSlide() {
    listTeamInfo.forEach((el, i) => (i === currentIndex) ? el.classList.add('remove-slide-info') : el.classList.remove('remove-slide-info'));
    setTimeout(() => {
        currentIndex = (currentIndex - 1 + listPicTeam.length) % listPicTeam.length
        showSlide(currentIndex)
    }, 1000)
}

function showSlide(index) {
    listPicTeam.forEach((elem, i) => {
        if (i === index) {
            elem.classList.add('active-slid')
            listTeamInfo.forEach(el => (el.dataset.info === elem.dataset.slid) ? el.classList.add('active-slide-info') : el.classList.remove('active-slide-info'))
        } else {
            elem.classList.remove('active-slid')
        }
    })
}


