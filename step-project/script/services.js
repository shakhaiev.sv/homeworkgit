'use strict'


let btnListServices = document.querySelector('.page-services .services-navbar');

btnListServices.addEventListener('click', clickMenuContent);

function clickMenuContent(event) {
    if (!event.target.closest('.services-navbar-item')) return;
    let intemListservices = btnListServices.querySelectorAll('li');
    let listContentService = document.querySelectorAll('.services-navbar-content li');
    [...intemListservices].map((element) => (event.target.dataset.title === element.dataset.title) ? element.classList.add('active-title') : element.classList.remove('active-title'));
    [...listContentService].map((element) => (event.target.dataset.title === element.dataset.content) ? element.classList.add('services-content-active') : element.classList.remove('services-content-active'));
};






