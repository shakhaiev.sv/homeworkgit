//gulp
import gulp from 'gulp';
const { src, dest, watch, series, parallel } = gulp;
import clean from 'gulp-clean';
import fileInclude from 'gulp-file-include';
import htmlmin from 'gulp-htmlmin';
import * as sassCompile from 'sass';
import sassgulp from 'gulp-sass';
const sass = sassgulp(sassCompile)
import autoprefixer from 'gulp-autoprefixer';
import cssClean from 'gulp-clean-css';
import imgmin from "gulp-imagemin";
// import uglify from "gulp-uglify";
import gulpUglify from "gulp-uglify-es";
const uglify = gulpUglify.default
import concat from "gulp-concat";
import bs from "browser-sync";
const browserSync = bs.create()


const path = {
    html: {
        src: './src/*.html',
        dist: './dist'
    },
    style: {
        src: './src/styles/**/*.scss',
        dist: './dist/style'
    },
    images: {
        src: "./src/images/**/*.{png,jpg,jpeg,gif,webp,svg}",
        dist: "./dist/images"
    },
    script: {
        src: "./src/script/**/*.js",
        dist: "./dist/script"
    }
};

const htmlTask = () => {
    return src(path.html.src)
        .pipe(fileInclude({
            prefix: '@@'
        }))
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(dest(path.html.dist))
};

const cleanTask = () => {
    return src('./dist', { allowEmpty: true })
        .pipe(clean())
};

const cssTask = () => {
    return src(path.style.src)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            cascade: false,
        }
        ))
        .pipe(cssClean({
            level: 2
        }))
        .pipe(dest(path.style.dist))
};

const imgminTask = () => {
    return src(path.images.src)
        .pipe(imgmin())
        .pipe(dest(path.images.dist))
};

const scriptsTask = () => {
    return src(path.script.src)
        .pipe(concat('script.js'))
        .pipe(uglify())
        .pipe(dest(path.script.dist))
};


const browserSyncTask = () => {
    browserSync.init({
        server: {
            baseDir: './dist'
        },
        open: true
    })
    watch('./src/**/*.html', series(htmlTask)).on('change', browserSync.reload)
    watch(path.style.src, series(cssTask)).on('change', browserSync.reload);
    watch(path.script.src, series(scriptsTask)).on('change', browserSync.reload);
    watch(path.images.src, series(imgminTask)).on('change', browserSync.reload);

}

export const build = series(cleanTask,
    parallel(htmlTask, cssTask, scriptsTask, imgminTask))

export const dev = series(build, browserSyncTask)