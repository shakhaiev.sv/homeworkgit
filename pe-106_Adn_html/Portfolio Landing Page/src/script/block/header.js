
window.addEventListener('scroll', function () {
    const header = document.querySelector('.header')
    if (window.scrollY > 92) {
        header.classList.add('header--bg')
    } else {
        header.classList.remove('header--bg')
    }
});


window.addEventListener('resize', function () {
    if (window.innerWidth > 1200) {
        const elbtn = document.querySelector('.header-nav__burg-btn')
        const menu = document.querySelector('.header-nav__menu');
        elbtn.classList.remove('header-nav__burg-btn--close')
        elbtn.firstChild.classList.remove('header-nav__burg-icon--none')
        menu.classList.remove("header-nav__menu--active")
    }
})



document.querySelector('.header-nav__burg-btn').addEventListener('click', function (e) {
    const elbtn = e.target.closest('.header-nav__burg-btn');
    const menu = document.querySelector('.header-nav__menu');
    console.log(elbtn.classList.contains('header-nav__burg-btn--close'));

    if (elbtn.classList.contains('header-nav__burg-btn--close')) {
        elbtn.classList.remove('header-nav__burg-btn--close')
        elbtn.firstChild.classList.remove('header-nav__burg-icon--none')
        menu.classList.remove("header-nav__menu--active")
    } else {

        elbtn.classList.add('header-nav__burg-btn--close')
        elbtn.firstChild.classList.add('header-nav__burg-icon--none')
        menu.classList.add("header-nav__menu--active")
    }
});