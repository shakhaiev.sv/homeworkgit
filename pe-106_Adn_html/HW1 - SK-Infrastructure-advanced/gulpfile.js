//gulp
import gulp from 'gulp';
const { src, dest, series, parallel, watch } = gulp;
import concat from 'gulp-concat';

//file include 
import fileInclude from 'gulp-file-include'; // переміщає файли 

// clean
import clean from 'gulp-clean'; //очищає каталоги

// css
import * as sass from 'sass';  //біблиотека для компіляции SASS файлов для CSS
import gulpSass from "gulp-sass";// біблиотека для використання бібліотек SASS (пакет в пакете от Олі)
const scss = gulpSass(sass); // компіляція стилів (SCSS в CSS)
import autoprefixer from "gulp-autoprefixer"; // робить автоматичні префікси
import csso from 'gulp-csso'; // перевірка і мініфікація вашого CSS

//imagemin 
import imgmin from 'gulp-imagemin';

//script
import uglify from 'gulp-uglify';

//browser-sync 
import browserSync from 'browser-sync';


// import imagemin from "gulp-imagemin"; // мініфікація картинок


// import bsc from "browser-sync"; // бібліотека-сервер для перевірки вашого проекту
// const browserSync = bsc.create(); // визов бібліотеки-сервера

const path = {
    dist: './dist',
    html: {
        src: './src/*.html',
        dist: './dist'
    },
    styles: {
        src: './src/styles/**/*.scss',
        dist: './dist/style'
    },
    img: {
        src: "./src/images/**/*.{png,jpg,jpeg,gif,webp,svg}",
        dist: "./dist/images"

    },
    script: {
        src: "./src/scripts/**/*.js",
        dist: "./dist/script"
    }
}


const htmlTask = () => {
    return src(path.html.src)
        .pipe(fileInclude())
        .pipe(dest(path.html.dist))
}

const cleanDist = () => {
    return src(path.dist, { allowEmpty: true })
        .pipe(clean())
}


const cssTask = () => {
    return src(path.styles.src)
        .pipe(scss().on('error', scss.logError))
        .pipe(autoprefixer({ cascade: false }))
        // .pipe(csso())
        .pipe(dest(path.styles.dist))

}

const imgTask = () => {
    return src(path.img.src)
        .pipe(imgmin())
        .pipe(dest(path.img.dist))
}

const scriptTask = () => {
    return src(path.script.src)
        .pipe(concat('script.min.js'))
        .pipe(uglify())
        .pipe(dest(path.script.dist))
}

// const browserSyncTask = () => {
//     browserSync.init({
//         server: {
//             baseDir: './dist'
//         },
//         open: true
//     })

//     watch('./dist/**/*.html').on('change',
//         series(htmlTask, browserSync.reload))
//     watch(path.styles.src).on('change', series(cssTask, browserSync.reload))
//     watch(path.script.src).on('change', series(scriptTask, browserSync.reload))
//     watch(path.img.src).on('change', series(imgTask, browserSync.reload))
// }


const browserSyncTask = () => {
    browserSync.init({
        server: {
            baseDir: "./dist",
        }
    });

    watch(path.styles.src).on(
        "all",
        series(cssTask, browserSync.reload)
    );
    watch('./src/**/*.html').on(
        "change",
        series(htmlTask, browserSync.reload)
    );
    watch(path.img.src).on(
        "all",
        series(imgTask, browserSync.reload)
    );
    watch(path.script.src).on(
        "all",
        series(scriptTask, browserSync.reload)
    );
};

export const build = series(cleanDist,
    parallel(htmlTask, cssTask, imgTask, scriptTask))

export const dev = series(build, browserSyncTask)

