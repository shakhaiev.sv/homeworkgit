import './Button.scss'
import cn from 'classnames'

const Button = (props) => {
    const { children, type, classNames, onClick, ...rest } = props;
    return (
        <button
            type={type}
            className={cn('btn', classNames)}
            onClick={onClick}
        >
            {children}
        </button>
    )
}

export default Button