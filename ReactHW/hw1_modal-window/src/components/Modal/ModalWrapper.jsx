


const ModalWrapper = ({ onClose, children }) => {
    return (
        <div className="modal-wrap" onClick={(e) => {
            if (e.target.classList.contains('modal-wrap')) {
                onClose()
            }
        }}>
            {children}
        </div>
    )
}

export default ModalWrapper