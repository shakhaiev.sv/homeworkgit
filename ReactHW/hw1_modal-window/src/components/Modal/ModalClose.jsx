import Close from './svg/close.svg?react'
import Button from '../Button/Button'

const ModalClose = ({ onClose }) => {

    return (
        <Button
            type='button'
            classNames='btn-close'
            onClick={onClose}
        >
            <Close />
        </Button>

    )
}

export default ModalClose