import Button from "../Button/Button.jsx"

const ModalFooter = (props) => {
    const { footer } = props
    return (
        <div className="modal-btn">
            {footer?.map((item, index) => {
                return <Button
                    key={index}
                    type="button"
                    classNames="btn-modal"
                    children={item.text}
                    onClick={item.click}
                />
            })}
        </div>
    )
}

export default ModalFooter