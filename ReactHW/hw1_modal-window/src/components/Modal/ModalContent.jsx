
const ModalContent = (props) => {
    const { img, imgClass, title, subTitle } = props.content;
    return (
        <div className="modal-content">
            {img && <img className={imgClass} src={img} alt="image" />}

            <h1 className="title">{title}</h1>
            <p className="subtitle">{subTitle}</p>
        </div>
    )
}
export default ModalContent