import ModalHeader from "./ModalHeader.jsx"
import ModalContent from "./ModalContent.jsx"
import ModalFooter from "./ModalFooter.jsx"

const ModalBody = (props) => {

    const { headerText, body, footer } = props.props

    return (
        <>

            {headerText && <ModalHeader> headerText</ModalHeader>}

            <ModalContent
                content={body}
            />
            <ModalFooter
                footer={footer}
            />
        </>
    )
}

export default ModalBody
