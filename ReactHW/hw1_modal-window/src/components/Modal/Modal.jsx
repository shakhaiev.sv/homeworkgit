import './Modal.scss'
import ModalWrapper from './ModalWrapper.jsx'
import ModalBody from './ModalBody.jsx'
import ModalClose from "./ModalClose.jsx"

export const Modal = (props) => {
    const { onClick, ...rest } = props
    return (
        <ModalWrapper onClose={onClick}>
            <div className='modal'>
                <ModalClose onClose={onClick} />
                <ModalBody props={rest} />
            </div>
        </ModalWrapper >
    )
}



