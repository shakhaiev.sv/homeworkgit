import { Modal } from "../Modal/Modal.jsx"
const ModalImage = ({ onClick }) => {
    return (
        <Modal
            onClick={onClick}
            // headerText='Base Modal'
            body={{
                "img": '/src/components/ModalPage/images/image.png',
                "imgClass": "image",
                "title": 'Product Delete!',
                "subTitle": "By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted."
            }}
            footer={[
                {
                    "text": 'NO, CANCEL',
                    "click": () => alert('Cancel')
                },
                {
                    "text": "YES, DELETE",
                    "click": () => alert('Delete')
                }
            ]}
        />
    )
}

export default ModalImage