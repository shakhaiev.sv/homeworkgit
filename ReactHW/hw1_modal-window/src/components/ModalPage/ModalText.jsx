import { Modal } from "../Modal/Modal.jsx"
const ModalText = ({ onClick }) => {
    return (
        <Modal
            onClick={onClick}
            body={{
                "title": 'Add Product “NAME”',
                "subTitle": "Description for you product"
            }}
            footer={[
                {
                    "text": 'ADD TO FAVORITE',
                    'click': () => alert('ADD TO FAVORITE')
                },
            ]}
        />
    )
}

export default ModalText