import React, { useState } from 'react'
import './App.scss'
import Button from './components/Button/Button.jsx'
import ModalImage from './components/ModalPage/ModalImage.jsx'
import ModalText from './components/ModalPage/ModalText.jsx'

function App() {
  const [isFirstModal, setIsFirstModal] = useState(false)
  const [isSecondModal, setIsSecondModal] = useState(false)

  const handleToogleFirstModal = () => {
    setIsFirstModal(!isFirstModal)
  }

  const handleToogleSecondModal = () => {
    setIsSecondModal(!isSecondModal)
  }


  return (
    <>
      <div className="container">
        <Button type="button" classNames="btn-open" onClick={handleToogleFirstModal}  >
          Open first modal
        </Button>
        <Button type="button" classNames="btn-open" onClick={handleToogleSecondModal} >
          Open second modal
        </Button>
      </div>
      {isFirstModal && <ModalImage onClick={handleToogleFirstModal} />}
      {isSecondModal && <ModalText onClick={handleToogleSecondModal} />}
    </>



  )
}

export default App
