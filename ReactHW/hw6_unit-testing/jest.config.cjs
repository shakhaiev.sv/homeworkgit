module.exports = {
    testEnvironment: "jsdom",
    moduleNameMapper: {
        "^.+\\.(css|less|scss)$": "identity-obj-proxy",
        "\\.(svg)$": "<rootDir>/__mocks__/svg.cjs",
        "\\.svg?react": "<rootDir>/__mocks__/svg.js",
        "\\.svg": "<rootDir>/__mocks__/svg.js",
        "^.+\\.svg$": "jest-svg-transformer",
    },
    transform: {
        "^.+\\.(ts|js|jsx)$": "babel-jest",
        "^.+\\.(svg|css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$": "<rootDir>/__mocks__/jest.transform.cjs"
    },
    transformIgnorePatterns: ["/node_modules/(?!swiper|swiper/react|ssr-window|dom7)"],
    setupFilesAfterEnv: [
        "<rootDir>/setupTests.js"
    ]
}
