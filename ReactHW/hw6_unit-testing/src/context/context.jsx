import { createContext, useState } from "react"

export const Context = createContext({})

const ContextProvider = ({ children }) => {

    const [toogleCardVieW, setToogleCardVieW] = useState(false)

    const handleToogleCardVieW = () => {
        setToogleCardVieW(!toogleCardVieW)
    }

    return (
        <Context.Provider
            value={{
                toogleCardVieW,
                handleToogleCardVieW
            }}>

            {children}
        </Context.Provider>)
}

export default ContextProvider