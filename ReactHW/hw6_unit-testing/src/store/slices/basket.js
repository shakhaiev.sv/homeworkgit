import { createSlice } from '@reduxjs/toolkit'


const initialState = {
    listBasket: [],
    isOpenModalAddBasket: false,
    isOpenModalRemoveBasket: false,
    descriptionModal: {},
    allLengthGoodsOnBasket: undefined,
    allSummGoodsOnBasket: undefined,
    savings: undefined,
    shipping: undefined,
}

const BasketSlice = createSlice({
    name: 'basket',
    initialState,
    reducers: {
        actionAddBasketList: (state, { payload }) => {
            if (state.listBasket.some((item) => item.nameID === payload.nameID && item.article === payload.article)) {
                state.listBasket = state.listBasket.map((item) => {
                    if (item.nameID === payload.nameID && item.article === payload.article) {
                        item.quantity = item.quantity + 1
                        return item
                    }
                    return item
                })
                localStorage.setItem('listBasket', JSON.stringify(state.listBasket))
            } else {
                state.listBasket = [...state.listBasket, { ...payload, 'quantity': 1 }]
                localStorage.setItem('listBasket', JSON.stringify(state.listBasket))
            }
        },
        actiohSetListBasketFromLocalSrorage: (state, action) => {
            const listBasketFromLS = localStorage.getItem('listBasket')
            if (listBasketFromLS) {
                state.listBasket = [...JSON.parse(listBasketFromLS)]
            }
        },
        actionIsOpenModalAddBasket: (state) => {
            state.isOpenModalAddBasket = !state.isOpenModalAddBasket
        },
        actionCreatedModal: (state, { payload }) => {
            state.descriptionModal = { ...payload }
        },
        actionIsOpenModalRemoveBasket: (state) => {
            state.isOpenModalRemoveBasket = !state.isOpenModalRemoveBasket
        },
        actionRemoveCardFromBasketList: (state, { payload }) => {
            state.listBasket = [...state.listBasket].filter((item) => item.article !== payload.article)
            localStorage.setItem('listBasket', JSON.stringify(state.listBasket))
        },
        actionAllLengthCoodsOnBasket: (state) => {
            state.allLengthGoodsOnBasket = 0

            state.listBasket.forEach((item) => {
                if (item.hasOwnProperty('quantity')) {
                    state.allLengthGoodsOnBasket = state.allLengthGoodsOnBasket + item['quantity']
                } else {
                    state.allLengthGoodsOnBasket += 1
                }

            })
        },
        actionAllSummGoodsOnBasket: (state) => {
            state.allSummGoodsOnBasket = 0

            state.listBasket.forEach((item) => {
                state.allSummGoodsOnBasket += item.quantity * Number(item.price)
            })

        },
        actionRandomSavings: (state) => {
            state.savings = Math.floor(Math.random() * 10)
        },
        actionRandomShipping: (state) => {
            state.shipping = Math.floor(Math.random() * 10)
        },
        actionDeleteGoodsOnBasket: (state) => {
            state.listBasket.length = 0
        }
    },
})

export const {
    actionAddBasketList,
    actiohSetListBasketFromLocalSrorage,
    actionIsOpenModalAddBasket,
    actionIsOpenModalRemoveBasket,
    actionCreatedModal,
    actionRemoveCardFromBasketList,
    actionAllLengthCoodsOnBasket,
    actionAllSummGoodsOnBasket,
    actionRandomSavings,
    actionRandomShipping,
    actionDeleteGoodsOnBasket

} = BasketSlice.actions

export default BasketSlice.reducer