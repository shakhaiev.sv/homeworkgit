import basketSlice, {
    actionAddBasketList,
    actiohSetListBasketFromLocalSrorage,
    actionIsOpenModalAddBasket,
    actionIsOpenModalRemoveBasket,
    actionCreatedModal,
    actionRemoveCardFromBasketList,
    actionAllLengthCoodsOnBasket
} from './basket.js'


describe('testing reducer basket', () => {

    test("testing initialstate  empty", () => {
        expect(basketSlice(undefined, { type: undefined })).toEqual({
            listBasket: [],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        })
    })

    test("testing actionAddBasketList and state", () => {

        const prevState = {
            listBasket: [],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        }

        expect(basketSlice(prevState, actionAddBasketList({
            id: 5858,
            nameID: 'shirt',
            name: "Shirt",
            price: 222,
            article: 911
        }))).toStrictEqual({
            listBasket: [{ id: 5858, nameID: 'shirt', name: "Shirt", price: 222, article: 911, quantity: 1 }],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        })
    })

    test("testing actionAddBasketList and state new card", () => {

        const prevState = {
            listBasket: [
                { id: 5858, nameID: 'shirt', name: "Shirt", price: 222, article: 911, quantity: 1 }
            ],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        }

        expect(basketSlice(prevState, actionAddBasketList({
            id: 585,
            nameID: 'hoodie',
            name: "Hoodie",
            price: 1222,
            article: 91,
        }))).toStrictEqual({
            listBasket: [
                { id: 5858, nameID: 'shirt', name: "Shirt", price: 222, article: 911, quantity: 1 },
                { id: 585, nameID: 'hoodie', name: "Hoodie", price: 1222, article: 91, quantity: 1 }
            ],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        })




    })

    test("testing actionAddBasketList and state on double card = quantity +1", () => {

        const prevState = {
            listBasket: [
                { id: 5858, nameID: 'shirt', name: "Shirt", price: 222, article: 911, quantity: 1 },
                { id: 585, nameID: 'hoodie', name: "Hoodie", price: 1222, article: 91, quantity: 1 }

            ],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        }

        expect(basketSlice(prevState, actionAddBasketList({
            id: 5858,
            nameID: 'shirt',
            name: "Shirt",
            price: 222,
            article: 911
        }))).toStrictEqual({
            listBasket: [
                { id: 5858, nameID: 'shirt', name: "Shirt", price: 222, article: 911, quantity: 2 },
                { id: 585, nameID: 'hoodie', name: "Hoodie", price: 1222, article: 91, quantity: 1 }
            ],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        })

    })

    test("testing action reebot from localstorage is empty", () => {
        const prevState = {
            listBasket: [],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        }

        expect(basketSlice(prevState, actiohSetListBasketFromLocalSrorage())).toStrictEqual({
            listBasket: [
                { id: 5858, nameID: 'shirt', name: "Shirt", price: 222, article: 911, quantity: 2 },
                { id: 585, nameID: 'hoodie', name: "Hoodie", price: 1222, article: 91, quantity: 1 }
            ],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        })
    })

    test("testing actionIsOpenModalAddBasket  ", () => {
        const prevState = {
            listBasket: [],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        }

        expect(basketSlice(prevState, actionIsOpenModalAddBasket())).toStrictEqual({
            listBasket: [],
            isOpenModalAddBasket: true,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        })
    })

    test("testing actionIsOpenModalRemoveBasket  ", () => {
        const prevState = {
            listBasket: [],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        }

        expect(basketSlice(prevState, actionIsOpenModalRemoveBasket())).toStrictEqual({
            listBasket: [],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: true,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        })
    })

    test("testing actionCreatedModal", () => {
        const prevState = {
            listBasket: [],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        }

        expect(basketSlice(prevState, actionCreatedModal({
            id: 5858,
            nameID: 'shirt',
            name: "Shirt",
            price: 222,
            article: 911
        }))).toStrictEqual({
            listBasket: [],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {
                id: 5858,
                nameID: 'shirt',
                name: "Shirt",
                price: 222,
                article: 911
            },
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        })

    })

    test("testing actionRemoveCardFromBasketList ", () => {

        const prevState = {
            listBasket: [
                { id: 5858, nameID: 'shirt', name: "Shirt", price: 222, article: 911, quantity: 1 },
                { id: 585, nameID: 'hoodie', name: "Hoodie", price: 1222, article: 91, quantity: 1 }
            ],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        }

        expect(basketSlice(prevState, actionRemoveCardFromBasketList({
            id: 5858,
            nameID: 'shirt',
            name: "Shirt",
            price: 222,
            article: 911
        }))).toStrictEqual({
            listBasket: [
                { id: 585, nameID: 'hoodie', name: "Hoodie", price: 1222, article: 91, quantity: 1 }
            ],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        })


    })

    test("testing action reebot from localstorage after actionRemoveCardFromBasketList  ", () => {
        const prevState = {
            listBasket: [],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        }

        expect(basketSlice(prevState, actiohSetListBasketFromLocalSrorage())).toStrictEqual({
            listBasket: [
                { id: 585, nameID: 'hoodie', name: "Hoodie", price: 1222, article: 91, quantity: 1 }
            ],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        })
    })

    test("testing actionAllLengthCoodsOnBasket", () => {
        const prevState = {
            listBasket: [
                { id: 5858, nameID: 'shirt', name: "Shirt", price: 222, article: 911, quantity: 2 },
                { id: 585, nameID: 'hoodie', name: "Hoodie", price: 1222, article: 91, quantity: 3 },
                { id: 58, nameID: 'polo', name: "Polo", price: 422, article: 9141, quantity: 1 },
                { id: 448, nameID: 'switer', name: "Switer", price: 922, article: 911141 }
            ],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: undefined,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        }

        expect(basketSlice(prevState, actionAllLengthCoodsOnBasket())).toStrictEqual({
            listBasket: [
                { id: 5858, nameID: 'shirt', name: "Shirt", price: 222, article: 911, quantity: 2 },
                { id: 585, nameID: 'hoodie', name: "Hoodie", price: 1222, article: 91, quantity: 3 },
                { id: 58, nameID: 'polo', name: "Polo", price: 422, article: 9141, quantity: 1 },
                { id: 448, nameID: 'switer', name: "Switer", price: 922, article: 911141 }
            ],
            isOpenModalAddBasket: false,
            isOpenModalRemoveBasket: false,
            descriptionModal: {},
            allLengthGoodsOnBasket: 7,
            allSummGoodsOnBasket: undefined,
            savings: undefined,
            shipping: undefined,
        })
    })
    // test("",()=>{})
    // test("",()=>{})
    // test("",()=>{})
    // test("",()=>{})
    // test("",()=>{})
    // test("",()=>{})



})