import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { sendRequest } from "../../helpers/sendRequest";
import { API_URL_CATEGORY_WOMEN } from '../../constans/API.js'

const initialState = {
    listCategoryWomen: [],
    listCategoryForWomenCards: {}
}

export const actionFetchCategoryForWomen = createAsyncThunk(
    'category/fetchCategoryForWomen',
    async () => {
        const data = await sendRequest(API_URL_CATEGORY_WOMEN)
        return data
    }
)

export const actionFetchCategoryWomenItem = createAsyncThunk(
    'categoryForWomen/cardItem',
    async (path) => {
        const data = await sendRequest(`../../public/categoryForWomen/${path}.json`)
        return data
    }
)


const categoryForWomenSlice = createSlice({
    name: 'categoryForWomen',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(actionFetchCategoryForWomen.fulfilled, (state, { payload }) => {
                state.listCategoryWomen = [...payload]
            })
            .addCase(actionFetchCategoryWomenItem.fulfilled, (state, action) => {
                const numberLink = action.meta.arg.slice(action.meta.arg.lastIndexOf('/') + 1)
                state.listCategoryForWomenCards[numberLink] = action.payload
            })

    }
})



export default categoryForWomenSlice.reducer