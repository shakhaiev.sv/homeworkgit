import favoriteSlice, {
    actionAddFavoriteList,
    actiohSetListFavoriteFromLocalSrorage
} from './favorite.js'


describe('testing reducer favorite', () => {

    test('testing initial state  empty', () => {
        expect(favoriteSlice(undefined, { type: undefined })).toEqual({ listFavorite: [] })
    })

    test('testing actionAddFavoriteList and state ', () => {

        const prevState = {
            listFavorite: []
        }
        expect(favoriteSlice(prevState, actionAddFavoriteList(
            { id: 5858, name: "Shirt", price: 222, article: 911 }
        ))).toStrictEqual(
            { listFavorite: [{ id: 5858, name: "Shirt", price: 222, article: 911 }] }
        )

    })

    test('testing actionAddFavoriteList delete on array double card', () => {


        const prevState = {
            listFavorite: [
                { id: 5858, name: "Shirt", price: 222, article: 911 },
                { id: 548, name: "Hoodie", price: 1222, article: 912 }
            ]
        }

        expect(favoriteSlice(prevState, actionAddFavoriteList(
            { id: 548, name: "Hoodie", price: 1222, article: 912 }
        ))).toStrictEqual({
            listFavorite: [{ id: 5858, name: "Shirt", price: 222, article: 911 }]
        })



    })

    test("action reboot from localstorage is empty", () => {
        const prevState = {
            listFavorite: []
        }

        expect(favoriteSlice(prevState, actiohSetListFavoriteFromLocalSrorage())).toStrictEqual({
            listFavorite: [{ id: 5858, name: "Shirt", price: 222, article: 911 }]
        })
    })

})