import './Footer.scss'
import Container from "../layout/Container/Container"
import Facebook from './images/svg/facebook.svg?react'
import Instagram from './images/svg/instagram.svg?react'
import Twitter from './images/svg/twiiter.svg?react'

const Footer = () => {
    return (
        <footer className='footer'>
            <Container>
                <div className="footer-wrap">
                    <ul className='help-list'>
                        <li className='help-item  help-title '>
                            <h3 className='help-item__title'>Need Help</h3>
                        </li>
                        <li className='help-item'>
                            <a className='help-link' href="#">Contact Us</a>
                        </li>
                        <li className='help-item'>
                            <a className='help-link' href="#">Track Order</a>
                        </li>
                        <li className='help-item'>
                            <a className='help-link' href="#">Returns & Refunds</a>
                        </li>
                        <li className='help-item'>
                            <a className='help-link' href="#">FAQ's</a>
                        </li>
                        <li className='help-item'>
                            <a className='help-link' href="#">Career</a>
                        </li>

                    </ul>
                    <ul className='company-list'>
                        <li className='company-item company-title'>
                            <h3 className='company-item__title'>Company</h3>
                        </li>
                        <li className='company-item'>
                            <a className='company-link' href="#">About Us</a>
                        </li>
                        <li className='company-item'>
                            <a className='company-link' href="#">euphoria Blog</a>
                        </li>
                        <li className='company-item'>
                            <a className='company-link' href="#">euphoriastan</a>
                        </li>
                        <li className='company-item'>
                            <a className='company-link' href="#">Collaboration</a>
                        </li>
                        <li className='company-item'>
                            <a className='company-link' href="#">Media</a>
                        </li>
                    </ul>
                    <ul className='info-list'>
                        <li className='info-item info-title'>
                            <h3 className='info-item__title'>More Info</h3>
                        </li>
                        <li className='info-item'>
                            <a className='info-link' href="#">Term and Conditions</a>
                        </li>
                        <li className='info-item'>
                            <a className='info-link' href="#">Privacy Policy</a>
                        </li>
                        <li className='info-item'>
                            <a className='info-link' href="#">Shipping Policy</a>
                        </li>
                        <li className='info-item'>
                            <a className='info-link' href="#">Sitemap</a>
                        </li>
                    </ul>
                    <ul className='location-list'>
                        <li className='location-item location-title'>
                            <h3 className='location-item__title'>Location</h3>
                        </li>
                        <li className='location-item'>
                            <a className='location-link' href="mailto:support@euphoria.in">support@euphoria.in</a>
                        </li>
                        <li className='location-item'>
                            <a className='location-link' href="#">Eklingpura Chouraha, Ahmedabad Main Road</a>
                        </li>
                        <li className='location-item'>
                            <a className='location-link' href="#">(NH 8- Near Mahadev Hotel) Udaipur, India- 313002</a>
                        </li>
                    </ul>
                </div>
                <ul className='footer-list-messenger'>
                    <li className="list-messenger-item">
                        <a className='messenger-link ' href="#">
                            <Facebook />
                        </a>
                    </li>
                    <li className="list-messenger-item">
                        <a className='messenger-link ' href="#">
                            <Instagram />
                        </a>
                    </li>
                    <li className="list-messenger-item">
                        <a className='messenger-link ' href="#">
                            <Twitter />
                        </a>
                    </li>
                    <li className="list-messenger-item">
                        <a className='messenger-link link-linkedin ' href="#">
                            in
                        </a>
                    </li>
                </ul>
                <div className='footer-copyright'>
                    <p >Copyright &copy; 2023 Euphoria Folks Pvt Ltd. All rights reserved.</p>

                </div>
            </Container>
        </footer>
    )
}

export default Footer