import './CardCategories.scss'
import React from 'react';
import PropTypes from 'prop-types';
import Arrow from "./images/svg/Arrow.svg?react"
import Button from '../Button/Button';

const CardCategories = (props) => {

    const { card, path } = props

    const { id, urlImg, name: title } = card

    const linkPath = `/${path}/${id}`
    return (
        <li className='card-category'>
            <Button to={linkPath}>
                <img className='item-img' src={urlImg} alt="img" />
            </Button>
            <div className="card-desc">
                <div className='wraper'>
                    <h3 className='title'>{title}</h3>
                    <p className='text'>Explore Now!</p>
                </div>
                <Button to={linkPath} className='arrow'>
                    <Arrow />
                </Button>
            </div>

        </li>
    );
};

CardCategories.propTypes = {
    card: PropTypes.object

};

export default CardCategories;