import React from 'react';
import { Field, ErrorMessage } from 'formik'
import PropTypes from 'prop-types';
import cn from 'classnames'
import "./Input.scss"
import { PatternFormat } from 'react-number-format';


const Input = (props) => {
    const {
        type,
        placeholder,
        name,
        label,
        error,
        format,
        ...restProps
    } = props
    return (
        <label className={cn("form-item", { "has-validation": error })}>
            <p className='form-label'>{label}</p>
            {format ? <PatternFormat
                className='form-input'
                format={format}
                allowEmptyFormatting
                mask='_'
            /> : <Field
                type={type}
                className='form-input'
                name={name}
                placeholder={placeholder}
                {...restProps}
            />
            }
            {<ErrorMessage name={name} className='error-massage' component={"p"} />}
        </label>
    );
};

Input.propTypes = {

    type: PropTypes.string,
    placeholder: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    error: PropTypes.bool,
};

export default Input;