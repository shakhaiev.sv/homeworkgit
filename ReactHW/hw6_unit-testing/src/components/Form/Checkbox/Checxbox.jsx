import React from 'react';
import PropTypes from 'prop-types';
import { Field, ErrorMessage } from 'formik';
import cn from 'classnames'
import './Checkbox.scss'

const Checxbox = (props) => {

    const {
        type = 'checkbox',
        name,
        label,
        classLabel,
        error,
        ...restProps
    } = props




    return (
        <>
            <label className={cn(classLabel, { 'has-validation': error })}>
                <Field
                    type={type}
                    name={name}
                    className='form-input'
                    {...restProps}
                />
                {label}

            </label>
            <ErrorMessage name={name} className='error-message' component={"p"} />

        </>
    );
};

Checxbox.propTypes = {
    type: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
};

export default Checxbox;