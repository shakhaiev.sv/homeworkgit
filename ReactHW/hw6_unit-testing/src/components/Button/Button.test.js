
import { render, screen, fireEvent } from "@testing-library/react"
import React from "react";
import Button from "./Button.jsx";
import { BrowserRouter } from "react-router-dom";

const handlerClick = jest.fn()

describe('testing component Button', () => {
    test('children props', () => {
        render(<Button>test</Button>)
        expect(screen.getByText("test")).toBeInTheDocument()
    })
    test('default className', () => {
        render(<Button>test</Button>)
        expect(screen.getByText('test')).toHaveClass('btn')
    })
    test('className props', () => {
        render(<Button className='class-props'>test</Button>)
        expect(screen.getByText('test')).toHaveClass('class-props')
    })
    test('default type=button', () => {
        render(<Button>test</Button>)
        expect(screen.getByText('test')).toHaveAttribute('type', "button")
    })
    test('testing Link component  screen tag=a ', () => {
        const { container } = render(<BrowserRouter>
            <Button to='/link-test'>test</Button>
        </BrowserRouter>)
        const link = container.querySelector('a')
        expect(link).toBeInTheDocument()
    })
    test('testing Link component  props to', () => {
        render(<BrowserRouter>
            <Button to='/link-test'>test</Button>
        </BrowserRouter>)
        expect(screen.getByText('test')).toHaveAttribute('href', '/link-test')
    })
    test('testing Link component null default props', () => {
        render(<BrowserRouter>
            <Button to='/link-test'>test</Button>
        </BrowserRouter>)
        expect(screen.getByText('test')).not.toHaveAttribute('type', 'button')
    })
    test('onclick props', () => {
        const { container } = render(<BrowserRouter>
            <Button onClick={handlerClick}>test</Button>
        </BrowserRouter>)
        const btn = container.querySelector('button')
        fireEvent.click(btn)
        expect(handlerClick).toHaveBeenCalled()
    })

})
