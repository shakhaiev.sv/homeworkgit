import React from 'react'

const Menu = () => {
    return (
        <ul className="nav-menu">
            <li className="menu-item">
                <a href="#" className="menu-link">Shop</a>
            </li>
            <li className="menu-item">
                <a href="#" className="menu-link">Men</a>
            </li>
            <li className="menu-item">
                <a href="#" className="menu-link">Women</a>
            </li>
            <li className="menu-item">
                <a href="#" className="menu-link">Combos</a>
            </li>
            <li className="menu-item">
                <a href="#" className="menu-link">Joggers</a>
            </li>
        </ul>
    )
}

export default Menu