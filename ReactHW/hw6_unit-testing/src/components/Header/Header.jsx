import Navigation from "./Navigation"
import Container from "../layout/Container/Container"
import React from "react"


const Header = () => {
    return (
        <header className="header">

            <Container>
                <Navigation />
            </Container>
        </header>
    )
}

export default Header