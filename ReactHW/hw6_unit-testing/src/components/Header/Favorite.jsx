import Button from "../Button/Button"
import FavoriteIcon from './images/svg/favorite.svg?react'
import cn from 'classnames'
import PropTypes from 'prop-types'
import React from 'react'

const Favorite = (props) => {
    const { favorite, onClick, dbfavorite, to } = props
    // console.log('favorite item button');
    return (
        <Button type='button'
            className={cn('btn-goods', 'active', { "count-goods": favorite })}
            data-favorite={favorite}
            data-dbfavorite={dbfavorite}
            onClick={onClick}
            to={to}
        >
            <FavoriteIcon />
        </Button >
    )
}

Favorite.propTypes = {
    favorite: PropTypes.number,
    onClick: PropTypes.func,
    to: PropTypes.string,
    dbfavorite: PropTypes.bool
}


export default Favorite