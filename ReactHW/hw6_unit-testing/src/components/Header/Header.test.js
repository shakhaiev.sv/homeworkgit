import Navigation from "./Navigation"
import Container from "../layout/Container/Container"
import { render } from '@testing-library/react'
import React from "react"
import { Provider } from "react-redux"
import store from '../../store/index.js'
import { BrowserRouter } from "react-router-dom"


describe('testing Header', () => {
    test('snapshot header', () => {
        const header = render(<Provider store={store}>
            <BrowserRouter>
                <header className="header">
                    <Container>
                        <Navigation />
                    </Container>
                </header>
            </BrowserRouter>
        </Provider>
        )

        expect(header).toMatchSnapshot()
    })
})