import './Limelight.scss'
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Container from '../../layout/Container/Container'
import ModalBasket from '../../../components/ModalBasket/ModalBasket.jsx'
import ModalBasketRemove from '../../../components/ModalBasketRemove/ModalBasketRemove.jsx'
import Card from '../../../components/CardGood/Card.jsx'
import { Context } from "../../../context/context.jsx"
import { useContext } from 'react';
import cn from 'classnames'


import {
    selectorlistLimeLight,
    selectorListFavorite,
    selectorListBasket,
    selectorIsOpenModalRemoveBasket,
    selectorIsOpenModalAddBasket,
    selectorDescriptionModal
} from '../../../store/selectors/selectors.js'

import { actionFetchListLimelight } from '../../../store/slices/limelight.js'
import { actionIsOpenModalRemoveBasket, actionIsOpenModalAddBasket } from '../../../store/slices/basket.js'



const Limelight = () => {
    const [listCard, setListCard] = useState([])
    const dispatch = useDispatch()
    const listLimelight = useSelector(selectorlistLimeLight)
    const listFavorite = useSelector(selectorListFavorite)
    const listBasket = useSelector(selectorListBasket)
    const isModalAddBasket = useSelector(selectorIsOpenModalAddBasket)
    const isModalRemoveBasket = useSelector(selectorIsOpenModalRemoveBasket)
    const currentCard = useSelector(selectorDescriptionModal)

    const handleCloseModalAddBasket = () => { dispatch(actionIsOpenModalAddBasket()) }
    const handleCloseModalRemoveBasket = () => { dispatch(actionIsOpenModalRemoveBasket()) }

    useEffect(() => {
        dispatch(actionFetchListLimelight())
    }, [])

    useEffect(() => {
        setListCard(listLimelight || [])
    }, [listLimelight])

    const {
        toogleCardVieW,
        handleToogleCardVieW
    } = useContext(Context)

    const handleToogle = () => {
        handleToogleCardVieW()
    }

    return (
        <section className="sect-limelight">
            <Container>
                <div className='sect-wrap__card-view' >
                    <h1 className="sect-title sect-limelight__title">In The Limelight</h1>
                    <div className={cn('sect__card-view', { "card-view--line": toogleCardVieW }, { "card-view--point": !toogleCardVieW })}
                        onClick={handleToogle}
                    >
                        <div className='line-1'></div>
                        <div className='line-2'></div>
                        <div className='line-3'></div>
                    </div>
                </div>
                <ul className={cn('sect-limelight__list', 'list-card', { 'list-card--view-line': toogleCardVieW })} >
                    {listCard.map((item, index) => {
                        return <Card
                            key={index}
                            product={item}
                            favorite={listFavorite.some((product) => product.nameID === item.nameID && product.article === item.article)}
                            basket={listBasket.some((product) => product.nameID === item.nameID && product.article === item.article)}
                        />
                    })}
                </ul>

            </Container>
            {
                isModalAddBasket && <ModalBasket
                    onClose={handleCloseModalAddBasket}
                    currentCard={currentCard}
                />
            }
            {
                isModalRemoveBasket && <ModalBasketRemove
                    onClose={handleCloseModalRemoveBasket}
                    currentCard={currentCard}
                />
            }
        </section >
    );
}


export default Limelight