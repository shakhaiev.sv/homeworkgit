import InfoSection from './Info/InfoSection.jsx'
import Brands from './Brands/Brands.jsx'
import CategoriesMen from './CategoriesMen/CategoriesMen.jsx'
import CategoriesWomen from './CategoriesWomen/CategoriesWomen.jsx'
import Limelight from './Limelight/Limelight.jsx'
import PropTypes from 'prop-types'

const Main = (props) => {
    return (
        <>
            <InfoSection />
            <CategoriesMen />
            <CategoriesWomen />
            <Brands />
            <Limelight />
        </>
    )
}

Main.propTypes = {
    onFavorite: PropTypes.func,
    onCart: PropTypes.func,
    favorite: PropTypes.array,
    cart: PropTypes.array
}

export default Main