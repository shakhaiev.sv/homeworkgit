import { useSelector, useDispatch } from 'react-redux'
import Card from "../../components/CardGood/Card"
import Container from "../../components/layout/Container/Container"
import ModalBasket from '../../components/ModalBasket/ModalBasket.jsx'

import { Context } from "../../context/context.jsx"
import { useContext } from 'react';
import cn from 'classnames'
import {
    selectorListFavorite,
    selectorListBasket,
    selectorDescriptionModal,
    selectorIsOpenModalAddBasket
} from '../../store/selectors/selectors.js'

import { actionIsOpenModalAddBasket } from '../../store/slices/basket.js'
import './FavoritePage.scss'
import { Link } from "react-router-dom"



const FavoritePage = () => {
    const dispatch = useDispatch()
    const ListFavorite = useSelector(selectorListFavorite)
    const listBacket = useSelector(selectorListBasket)
    const isModalAddBasket = useSelector(selectorIsOpenModalAddBasket)
    const currentCard = useSelector(selectorDescriptionModal)


    const handleCloseModalAddBasket = () => { dispatch(actionIsOpenModalAddBasket()) }

    const {
        toogleCardVieW,
        handleToogleCardVieW
    } = useContext(Context)

    const handleToogle = () => {
        handleToogleCardVieW()
    }
    return (
        <section className="sect-favorite">
            <Container>

                <div className='sect-wrap__card-view' >
                    <h1 className="sect-title  sect-favorite__title">My favorite products </h1>
                    <div className={cn('sect__card-view', { "card-view--line": toogleCardVieW }, { "card-view--point": !toogleCardVieW })}
                        onClick={handleToogle}
                    >
                        <div className='line-1'></div>
                        <div className='line-2'></div>
                        <div className='line-3'></div>
                    </div>
                </div>

                <ul className={cn("list-favorite", 'list-card', { 'list-card--view-line': toogleCardVieW })} >
                    {ListFavorite.map((item, index) => {
                        return <Card
                            key={index}
                            product={item}
                            hideBtnDelete={true}
                            favorite={true}
                            basket={listBacket.some((product) => product.nameID === item.nameID && product.article === item.article)}

                        />
                    })}
                </ul>
                {!ListFavorite.length && <div className="list-favorite-empty">
                    <p className="list-favorite-empty__text">
                        Oops! Items are missing, check out our <Link to='/limelight' >best items</Link>
                    </p>
                </div>}
            </Container>
            {isModalAddBasket && <ModalBasket
                onClose={handleCloseModalAddBasket}
                currentCard={currentCard}
            />}
        </section>


    )
}

export default FavoritePage