import './ItemCategoryWomenCards.scss'
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Context } from "../../context/context.jsx"
import { useContext } from 'react';
import cn from 'classnames'

import Container from '../../components/layout/Container/Container.jsx'
import ModalBasket from '../../components/ModalBasket/ModalBasket.jsx'
import ModalBasketRemove from '../../components/ModalBasketRemove/ModalBasketRemove.jsx'
import Card from '../../components/CardGood/Card.jsx'

import {
    selectorlistCategoryForWomenCards,
    selectorListFavorite,
    selectorListBasket,
    selectorIsOpenModalAddBasket,
    selectorIsOpenModalRemoveBasket,
    selectorDescriptionModal
} from '../../store/selectors/selectors.js'

import { actionFetchCategoryWomenItem } from '../../store/slices/categoryWomen.js'
import { actionIsOpenModalAddBasket, actionIsOpenModalRemoveBasket } from '../../store/slices/basket.js'

import { createTitlePage } from '../../helpers/createTitlePage.js'

const ItemCategoryMenCards = () => {
    const [listCard, setListCard] = useState([])
    const { id: pathname } = useParams()
    const dispatch = useDispatch()

    const listCategoryForWomenItemCards = useSelector(selectorlistCategoryForWomenCards)
    const listFavorite = useSelector(selectorListFavorite)
    const listBasket = useSelector(selectorListBasket)
    const isModalAddBasket = useSelector(selectorIsOpenModalAddBasket)
    const isModalRemoveBasket = useSelector(selectorIsOpenModalRemoveBasket)
    const currentCard = useSelector(selectorDescriptionModal)


    const handleCloseModalAddBasket = () => { dispatch(actionIsOpenModalAddBasket()) }
    const handleCloseModalRemoveBasket = () => { dispatch(actionIsOpenModalRemoveBasket()) }

    useEffect(() => {
        dispatch(actionFetchCategoryWomenItem(pathname))
    }, [])

    useEffect(() => {
        setListCard(listCategoryForWomenItemCards[pathname] || [])
    }, [listCategoryForWomenItemCards])


    const {
        toogleCardVieW,
        handleToogleCardVieW
    } = useContext(Context)

    const handleToogle = () => {
        handleToogleCardVieW()
    }

    return (
        <section className='category-card-list'>
            <Container>
                <div className='sect-wrap__card-view' >
                    <h1 className="sect-title">{createTitlePage(pathname)}</h1>
                    <div className={cn('sect__card-view', { "card-view--line": toogleCardVieW }, { "card-view--point": !toogleCardVieW })}
                        onClick={handleToogle}
                    >
                        <div className='line-1'></div>
                        <div className='line-2'></div>
                        <div className='line-3'></div>
                    </div>
                </div>

                <ul className={cn('category-card-item', 'list-card', { 'list-card--view-line': toogleCardVieW })}>
                    {listCard.map((item, index) => {
                        return <Card
                            key={index}
                            product={item}
                            favorite={listFavorite.some((product) => product.nameID === item.nameID && product.article === item.article)}
                            basket={listBasket.some((product) => product.nameID === item.nameID && product.article === item.article)}
                        />
                    })}
                </ul>
            </Container>
            {isModalAddBasket && <ModalBasket
                onClose={handleCloseModalAddBasket}
                currentCard={currentCard}
            />}
            {isModalRemoveBasket && <ModalBasketRemove
                onClose={handleCloseModalRemoveBasket}
                currentCard={currentCard}
            />}

        </section>
    );
};


export default ItemCategoryMenCards;