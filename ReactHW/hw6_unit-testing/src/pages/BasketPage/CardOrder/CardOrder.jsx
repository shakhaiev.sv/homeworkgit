import React from 'react';
import PropTypes from 'prop-types';
import './CardOrder.scss'
import Favorite from '../../../components/Header/Favorite';
import Button from '../../../components/Button/Button.jsx';
import Delete from '../../../components/CardGood/images/svg/Delete.svg?react'
import {
    actionIsOpenModalRemoveBasket,
    actionCreatedModal,
} from '../../../store/slices/basket.js'
import { actionAddFavoriteList } from '../../../store/slices/favorite.js'
import { useDispatch } from 'react-redux';




const CardOrder = (props) => {
    const { product, favorite, showBtn } = props
    const { name: title, article, urlImg, brand, price, color, quantity } = product;

    const dispatch = useDispatch()

    const handleRemoveCardFromBasket = () => {
        dispatch(actionIsOpenModalRemoveBasket())
        dispatch(actionCreatedModal({
            ...product
        }))
    }

    const handleFavorite = (e) => {
        let elementFavoriteBtn = e.target.closest('.btn')

        elementFavoriteBtn.dataset.dbfavorite = !elementFavoriteBtn.dataset.dbfavorite
        dispatch(actionAddFavoriteList({
            ...product,
        }))
    }

    return (
        <li className='order-list__item line'>
            <img className='order-list__img' src={`../${urlImg}`} alt={title} />
            <div className='order-list__desc'>
                <p className='order-list__name-goods'>{`${color[0][0].toUpperCase() + color[0].slice(1)} ${title}`}
                    <span className='order-list__quantity' > x {quantity}</span>
                </p>
                <p className='order-list__color'>Color
                    <span className='order-list__select-color'> : {color[0][0].toUpperCase() + color[0].slice(1)}</span>
                </p>

            </div>
            <p className='order-list__summ'>
                {quantity ? `$${quantity * price}` : `$${price}`}
            </p>
            <div className='btn-wrap'>
                {showBtn && <>
                    <Favorite
                        dbfavorite={favorite === undefined ? false : favorite}
                        onClick={handleFavorite}
                    />
                    <Button
                        onClick={handleRemoveCardFromBasket}
                        className='btn-goods'>
                        <Delete />
                    </Button>
                </>
                }
            </div>
        </li>
    );
};

CardOrder.propTypes = {
    product: PropTypes.object,
    quantity: PropTypes.number,
};

export default CardOrder;