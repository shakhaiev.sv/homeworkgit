
import { useEffect, useState } from 'react'
import './App.scss'
import Footer from './components/Footer/Footer.jsx'
import Header from './components/Header/Header.jsx'
import Main from './components/Main/Main.jsx'


function App() {

  const [cart, setCart] = useState([])

  const handleCart = (card) => {
    return setCart((cart) => {
      let result;

      if (card.hasOwnProperty('deleteCartArticle')) {

        const listCartLS = JSON.parse(localStorage.getItem('listCast'))

        result = listCartLS.filter((item) => item.article !== card.deleteCartArticle)
        localStorage.setItem('listCast', JSON.stringify(result))
      } else {

        result = [...cart, card]
        localStorage.setItem('listCast', JSON.stringify(result))
      }
      return result
    })
  }



  const [favorite, setFavorite] = useState([])

  const handleFavorite = (card) => {
    return setFavorite((favorite) => {
      let result;

      if (card.hasOwnProperty('deleteCartArticle')) {

        const listFavoriteLS = JSON.parse(localStorage.getItem('listFavorite'))
        result = listFavoriteLS.filter((item) => item.article !== card.deleteCartArticle)
        localStorage.setItem('listFavorite', JSON.stringify(result))

      } else {

        result = [...favorite, card]
        localStorage.setItem('listFavorite', JSON.stringify(result))
      }

      return result
    })
  }



  useEffect(() => {
    const listFavorite = localStorage.getItem('listFavorite')
    const listCast = localStorage.getItem('listCast')
    if (!cart.length && listCast) {
      setCart(JSON.parse(listCast))
    }
    if (!favorite.length && listFavorite) {
      setFavorite(JSON.parse(listFavorite))
    }
  }, [])



  return (
    <>
      <Header
        cart={cart.length}
        favorite={favorite.length}

      />
      <Main
        cart={cart}
        favorite={favorite}
        onCart={handleCart}
        onFavorite={handleFavorite}
      />
      <Footer />
    </>
  )
}

export default App
