import './Card.scss'
import Arrow from "./images/svg/Arrow.svg?react"
import cn from 'classnames'
import Favorite from '../Header/Favorite.jsx'
import Basket from '../Header/Basket.jsx'
import PropTypes from 'prop-types'


const Card = (props) => {
    const {
        className,
        showModalCard,
        handleCurrentCard,
        onCart,
        onFavorite,
        favorite,
        product,
        cart } = props;

    const { color, name: title, article, urlImg, brand, price, availabilityColor } = product;

    const doubleFavorite = favorite?.some((item) => item.article === article)
    const doubleCart = cart?.some((item) => item.article === article)

    const dbFavoriteClass = favorite?.find((item) => item.article === article)

    const dbCartClass = cart?.find((item) => item.article === article)


    return (
        <li
            className={cn('card', className)}
            onClick={(e) => {
                handleCurrentCard({
                    'urlImg': urlImg,
                    'title': title,
                    "price": price,
                    "brand": brand,
                    'article': article,
                    "item": e.target.closest('.card')
                })
                if (!e.target.closest('.btn-goods')) {
                    showModalCard()
                }
            }}
        >
            <img className='item-img' src={urlImg} alt="img" />

            <div className="card-desc">
                <div className='wraper'>
                    <h3 className='title'>{title}</h3>
                    {(brand) ? <p className='brand'>{brand}</p> : <p className='text'>Explore Now!</p>}
                </div>
                {(price) ? <a
                    className='price'
                    href="#">${price}
                </a> : <a
                    className='arrow'
                    href="#">
                    <Arrow />
                </a>}
            </div>
            {availabilityColor && <div
                className='card-add-favorite'>
                <div className="color">
                    {color.map((item, index) => <a href="#" key={index} style={{
                        backgroundColor: item,
                        width: '15px',
                        height: '15px',
                        borderRadius: "50%",
                        display: 'block'
                    }}></a>)}

                </div>
                <div className='btn-wrap'>
                    <Favorite
                        onClick={(e) => {
                            if (favorite) {
                                if (doubleFavorite) {
                                    // видалити картку 
                                    onFavorite({
                                        'deleteCartArticle': article
                                    })
                                    e.target.closest('.btn').dataset.dbfavorite = false

                                } else {
                                    // додати картку 
                                    e.target.closest('.btn').dataset.dbfavorite = true
                                    onFavorite({
                                        'urlImg': urlImg,
                                        'title': title,
                                        "price": price,
                                        "brand": brand,
                                        'article': article,
                                        'dbfavorite': e.target.closest('.btn').dataset.dbfavorite
                                    })
                                }
                            }
                        }}

                        dbfavorite={dbFavoriteClass ? dbFavoriteClass.dbfavorite : false}
                    />
                    <Basket
                        onClick={(e) => {
                            if (cart) {
                                if (doubleCart) {
                                    // видалити картку 
                                    onCart({
                                        'deleteCartArticle': article
                                    })
                                    e.target.closest('.btn').dataset.dbattrcart = false
                                } else {
                                    // додати картку 
                                    e.target.closest('.btn').dataset.dbattrcart = true
                                    onCart({
                                        'urlImg': urlImg,
                                        'title': title,
                                        "price": price,
                                        "brand": brand,
                                        'article': article,
                                        'dbattrcart': e.target.closest('.btn').dataset.dbattrcart
                                    })
                                }
                            }
                        }}

                        dbattrcart={dbCartClass ? dbCartClass.dbattrcart : false}
                    />
                </div>
            </div>}
        </li>
    )
}


Card.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
    showModalCard: PropTypes.func,
    handleCurrentCard: PropTypes.func,
    onCart: PropTypes.func,
    onFavorite: PropTypes.func,
    product: PropTypes.object,
    favorite: PropTypes.array,
    cart: PropTypes.array
}

export default Card