import './Limelight.scss'
import { useState } from 'react'
import Container from '../../layout/Container/Container'
import LisrCard from '../../ListCard/ListCard'
import ModalCard from '../../ModalCard/ModalCard'
import PropTypes from 'prop-types'


const Limelight = (props) => {
    const { onFavorite, onCart, favorite, cart } = props

    const [isModal, setIsModal] = useState(false)

    const handleModalCard = () => {
        setIsModal(!isModal)
    }

    // для заповнення модалки обєкт
    const [currentCard, setCurrentCard] = useState({})
    const handleCurrentCard = (card) => {
        setCurrentCard(card)
    }



    return (
        <section className="sect-limelight">
            <Container>
                <h1 className="sect-title sect-limelight__title">In The Limelight</h1>
                <LisrCard
                    classUl="sect-limelight__list"
                    classLi='sect-limelight__item'
                    url='../../../../public/goods/limelight.json'
                    showModalCard={handleModalCard}
                    handleCurrentCard={handleCurrentCard}
                    onCart={onCart}
                    onFavorite={onFavorite}
                    favorite={favorite}
                    cart={cart}

                />
            </Container>
            {isModal && <ModalCard
                onClose={handleModalCard}
                currentCard={currentCard}
                onCart={onCart}
                onFavorite={onFavorite}
                favorite={favorite}
                cart={cart}

            />}
        </section>
    )
}

Limelight.propTypes = {
    onFavorite: PropTypes.func,
    onCart: PropTypes.func,
    favorite: PropTypes.array,
    cart: PropTypes.array
}

export default Limelight