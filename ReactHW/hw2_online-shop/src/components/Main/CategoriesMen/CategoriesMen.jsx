
import './CategoriesMen.scss'
import Container from '../../layout/Container/Container'
import LisrCard from '../../ListCard/ListCard'


const CategoriesMen = () => {
    return (
        <section className="sect-categ-men">
            <Container>
                <h1 className="sect-title sect-categ-men__title">Categories For Men</h1>
                <LisrCard
                    classUl="categor-men-list"
                    classLi='categor-men-item'
                    url='../../../../public/goods/categoriesForMen.json'
                />
            </Container>
        </section>
    )
}

export default CategoriesMen