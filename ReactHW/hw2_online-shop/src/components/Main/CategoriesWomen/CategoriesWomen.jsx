
import './CategoriesWomen.scss'
import Container from '../../layout/Container/Container'
import LisrCard from '../../ListCard/ListCard'


const CategoriesWomen = () => {
    return (
        <section className="sect-categ-women">
            <Container>
                <h1 className="sect-title sect-categ-women__title">Categories For Women</h1>
                <LisrCard
                    classUl="categor-women-list"
                    classLi='categor-women-item'
                    url='../../../../public/goods/categoriesForWomen.json'
                />
            </Container>
        </section>
    )
}

export default CategoriesWomen