import InfoSection from '../Main/Info/InfoSection.jsx'
import Brands from './Brands/Brands.jsx'
import CategoriesMen from './CategoriesMen/CategoriesMen.jsx'
import CategoriesWomen from './CategoriesWomen/CategoriesWomen.jsx'
import Limelight from './Limelight/Limelight.jsx'
import PropTypes from 'prop-types'

const Main = (props) => {
    const { onCart, onFavorite, favorite, cart } = props
    return (
        <main>
            <InfoSection />
            <CategoriesMen />
            <CategoriesWomen />
            <Brands />
            <Limelight
                favorite={favorite}
                cart={cart}
                onCart={onCart}
                onFavorite={onFavorite}

            />
        </main>
    )
}

Main.propTypes = {
    onFavorite: PropTypes.func,
    onCart: PropTypes.func,
    favorite: PropTypes.array,
    cart: PropTypes.array
}

export default Main