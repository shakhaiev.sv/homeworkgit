import './Button.scss'
import PropTypes from 'prop-types'
import cn from 'classnames'


const Button = (props) => {

    const {
        type = 'button',
        className,
        children,
        onClick,
        ...restProps
    } = props


    return (
        <button
            onClick={onClick}
            type={type}
            className={cn("btn", className)}
            {...restProps}
        >
            {children}
        </button>)
}


Button.propTypes = {
    type: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func
}

export default Button