import "./Header.scss"
import Logo from './Logo'
import Menu from './Menu'
import Search from "./Search"
import Basket from "./Basket.jsx"
import Favorite from "./Favorite.jsx"
import PropTypes from 'prop-types'

const Navigation = (props) => {
    const { cart, favorite } = props
    return (
        <nav className="header-nav">
            <Logo />
            <Menu />
            <Search />
            <div className="header-goods">
                <Basket
                    cart={cart} />

                <Favorite
                    favorite={favorite} />
            </div>

        </nav>
    )
}

Navigation.propTypes = {
    cart: PropTypes.number,
    favorite: PropTypes.number,
}

export default Navigation