import Button from "../Button/Button"
import BasketIcon from './images/svg/basket.svg?react'
import cn from 'classnames'
import PropTypes from 'prop-types'


const Basket = (props) => {
    const { cart, onClick, dbattrcart } = props
    return (
        <Button type='button'
            data-dbattrcart={dbattrcart}
            className={cn('btn-goods', 'active', { "count-goods": cart })}
            data-cart={cart}
            onClick={onClick}
        >
            <BasketIcon />
        </Button>
    )
}

Basket.propTypes = {
    cart: PropTypes.number,
    onClick: PropTypes.func
}

export default Basket