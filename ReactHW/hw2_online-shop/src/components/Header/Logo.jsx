

const Logo = () => {

    return (
        <a href="#" className="header-logo">
            <img src="./public/images/header/logo.png" className="header-img" alt="euphoria" />
        </a>
    )
}

export default Logo