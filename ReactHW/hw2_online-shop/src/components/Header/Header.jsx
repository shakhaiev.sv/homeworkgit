import Navigation from "./Navigation"
import Container from "../layout/Container/Container"
import PropTypes from 'prop-types'


const Header = (props) => {
    const { cart, favorite } = props;

    return (
        <header className="header">
            <Container>
                <Navigation
                    cart={cart}
                    favorite={favorite}
                />
            </Container>
        </header>
    )
}

Header.propTypes = {
    cart: PropTypes.number,
    favorite: PropTypes.number,
}


export default Header