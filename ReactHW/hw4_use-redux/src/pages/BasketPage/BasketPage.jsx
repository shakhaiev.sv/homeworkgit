import { useEffect, useState } from 'react'
import './BasketPage.scss'
import Card from '../../components/CardGood/Card.jsx'
import Container from '../../components/layout/Container/Container.jsx'
import { Link } from "react-router-dom"
import {
    selectorListBasket,
    selectorListFavorite,
    selectorIsOpenModalRemoveBasket,
    selectorDescriptionModal
} from '../../store/selectors/selectors.js'
import { useSelector, useDispatch } from 'react-redux'
import { actionIsOpenModalRemoveBasket } from '../../store/slices/basket.js'
import { mergeTwoArray } from '../../helpers/mergeTwoArray.js'
import ModalBasketRemove from '../../components/ModalBasketRemove/ModalBasketRemove.jsx'


const BasketPage = () => {
    const [listCard, setListCard] = useState([])
    const dispatch = useDispatch()
    const listBacket = useSelector(selectorListBasket)
    const listFavorite = useSelector(selectorListFavorite)
    const isModalRemoveBasket = useSelector(selectorIsOpenModalRemoveBasket)
    const currentCard = useSelector(selectorDescriptionModal)
    const handleCloseModalRemoveBasket = () => { dispatch(actionIsOpenModalRemoveBasket()) }

    useEffect(() => {
        const listCurrent = mergeTwoArray('basket', listBacket, listFavorite)
        setListCard(listCurrent || [])
    }, [listBacket, listFavorite])


    return (
        <section className="sect-basket">
            <Container>
                <h1 className="sect-title  sect-basket__title">My basket products </h1>
                <ul className="list-basket">
                    {listCard.map((item, index) => {
                        return <Card
                            hideBtnBasket={true}
                            key={index}
                            product={item}

                        />
                    })}
                </ul>
                {!listCard.length && <div className="list-basket-empty">
                    <p className="list-basket-empty__text">
                        Oops! Items are missing, check out our <Link to='/limelight' >best items</Link>
                    </p>
                </div>}
            </Container>
            {isModalRemoveBasket && <ModalBasketRemove
                onClose={handleCloseModalRemoveBasket}
                currentCard={currentCard} />}
        </section>
    )
}


export default BasketPage