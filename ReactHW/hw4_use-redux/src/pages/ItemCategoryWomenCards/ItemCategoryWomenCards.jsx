import './ItemCategoryWomenCards.scss'
import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import Container from '../../components/layout/Container/Container.jsx'
import ModalBasket from '../../components/ModalBasket/ModalBasket.jsx'
import ModalBasketRemove from '../../components/ModalBasketRemove/ModalBasketRemove.jsx'
import Card from '../../components/CardGood/Card.jsx'

import {
    selectorlistCategoryForWomenCards,
    selectorListFavorite,
    selectorListBasket,
    selectorIsOpenModalAddBasket,
    selectorIsOpenModalRemoveBasket,
    selectorDescriptionModal
} from '../../store/selectors/selectors.js'

import { actionFetchCategoryWomenItem } from '../../store/slices/categoryWomen.js'
import { actionIsOpenModalAddBasket, actionIsOpenModalRemoveBasket } from '../../store/slices/basket.js'

import { mergeList } from '../../helpers/mergeList.js'
import { createTitlePage } from '../../helpers/createTitlePage.js'

const ItemCategoryMenCards = () => {
    const [listCard, setListCard] = useState([])

    const { pathname } = useLocation()
    const dispatch = useDispatch()

    const listCategoryForWomenItemCards = useSelector(selectorlistCategoryForWomenCards)
    const listFavorite = useSelector(selectorListFavorite)
    const listBasket = useSelector(selectorListBasket)
    const isModalAddBasket = useSelector(selectorIsOpenModalAddBasket)
    const isModalRemoveBasket = useSelector(selectorIsOpenModalRemoveBasket)
    const currentCard = useSelector(selectorDescriptionModal)

    const linkId = pathname.slice(pathname.lastIndexOf('/') + 1)

    const handleCloseModalAddBasket = () => { dispatch(actionIsOpenModalAddBasket()) }
    const handleCloseModalRemoveBasket = () => { dispatch(actionIsOpenModalRemoveBasket()) }



    useEffect(() => {
        dispatch(actionFetchCategoryWomenItem(pathname))
    }, [])

    useEffect(() => {
        const listCurrent = mergeList(listCategoryForWomenItemCards[linkId], listFavorite, listBasket)
        setListCard(listCurrent || [])
    }, [linkId, listFavorite, listBasket, listCategoryForWomenItemCards])

    return (
        <section className='category-card-list'>
            <Container>
                <h1 className="sect-title">{createTitlePage(linkId)}</h1>
                <ul className='category-card-item'>
                    {listCard.map((item, index) => {
                        return <Card
                            key={index}
                            product={item}
                        />
                    })}
                </ul>

            </Container>
            {isModalAddBasket && <ModalBasket
                onClose={handleCloseModalAddBasket}
                currentCard={currentCard}
            />}
            {isModalRemoveBasket && <ModalBasketRemove
                onClose={handleCloseModalRemoveBasket}
                currentCard={currentCard}
            />}

        </section>


    );
};


export default ItemCategoryMenCards;