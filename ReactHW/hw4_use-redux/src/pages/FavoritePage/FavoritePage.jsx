import { useEffect, useState } from "react"
import { useSelector, useDispatch } from 'react-redux'
import Card from "../../components/CardGood/Card"
import Container from "../../components/layout/Container/Container"
import ModalBasket from '../../components/ModalBasket/ModalBasket.jsx'
import ModalBasketRemove from '../../components/ModalBasketRemove/ModalBasketRemove.jsx'

import {
    selectorListFavorite,
    selectorListBasket,
    selectorIsOpenModalRemoveBasket,
    selectorDescriptionModal,
    selectorIsOpenModalAddBasket
} from '../../store/selectors/selectors.js'

import { actionIsOpenModalRemoveBasket, actionIsOpenModalAddBasket } from '../../store/slices/basket.js'
import './FavoritePage.scss'
import { Link } from "react-router-dom"

import { mergeTwoArray } from '../../helpers/mergeTwoArray.js'


const FavoritePage = () => {
    const [listCard, setListCard] = useState([])
    const dispatch = useDispatch()
    const ListFavorite = useSelector(selectorListFavorite)
    const listBacket = useSelector(selectorListBasket)
    const isModalAddBasket = useSelector(selectorIsOpenModalAddBasket)
    // const isModalRemoveBasket = useSelector(selectorIsOpenModalRemoveBasket)
    const currentCard = useSelector(selectorDescriptionModal)
    // const handleCloseModalRemoveBasket = () => { dispatch(actionIsOpenModalRemoveBasket()) }

    const handleCloseModalAddBasket = () => { dispatch(actionIsOpenModalAddBasket()) }

    useEffect(() => {
        const listCurrent = mergeTwoArray('favorite', listBacket, ListFavorite)
        setListCard(listCurrent)
    }, [ListFavorite, listBacket])

    return (
        <section className="sect-favorite">
            <Container>
                <h1 className="sect-title  sect-favorite__title">My basket products </h1>
                <ul className="list-favorite">
                    {listCard.map((item, index) => {
                        return <Card
                            key={index}
                            product={item}
                            hideBtnDelete={true}
                        />
                    })}
                </ul>
                {!listCard.length && <div className="list-favorite-empty">
                    <p className="list-favorite-empty__text">
                        Oops! Items are missing, check out our <Link to='/limelight' >best items</Link>
                    </p>
                </div>}
            </Container>
            {isModalAddBasket && <ModalBasket
                onClose={handleCloseModalAddBasket}
                currentCard={currentCard}
            />}
            {/* {isModalRemoveBasket && <ModalBasketRemove
                onClose={handleCloseModalRemoveBasket}
                currentCard={currentCard}
            />} */}
        </section>


    )
}

export default FavoritePage