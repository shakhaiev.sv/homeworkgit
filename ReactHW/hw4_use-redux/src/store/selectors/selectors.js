export const selectorCategoryForMen = (store) => store.categoryForMen.listCategoryMen
export const selectorlistCategoryForMenCards = (store) => store.categoryForMen.listCategoryForMenCards

export const selectorCategoryForWomen = (store) => store.categoryForWomen.listCategoryWomen
export const selectorlistCategoryForWomenCards = (store) => store.categoryForWomen.listCategoryForWomenCards


export const selectorListFavorite = (store) => store.favorite.listFavorite


export const selectorListBasket = (store) => store.basket.listBasket
export const selectorIsOpenModalAddBasket = (store) => store.basket.isOpenModalAddBasket
export const selectorIsOpenModalRemoveBasket = (store) => store.basket.isOpenModalRemoveBasket
export const selectorDescriptionModal = (store) => store.basket.descriptionModal

export const selectorlistLimeLight = (store) => store.limelight.listLimeLight
