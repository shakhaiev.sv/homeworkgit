import { configureStore } from '@reduxjs/toolkit'
import favoriteSlice from './slices/favorite.js'
import BasketSlice from './slices/basket.js'
import categoryForMenSlice from './slices/categoryMen.js'
import categoryForWomenSlice from './slices/categoryWomen.js'
import limelightSlice from './slices/limelight.js'

export default configureStore({
    reducer: {
        favorite: favoriteSlice,
        basket: BasketSlice,
        categoryForMen: categoryForMenSlice,
        categoryForWomen: categoryForWomenSlice,
        limelight: limelightSlice
    }
})
