import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { sendRequest } from "../../helpers/sendRequest";
import { API_URL_CATEGORY_MEN } from '../../constans/API.js'

const initialState = {
    listCategoryMen: [],
    listCategoryForMenCards: {}
}

export const actionFetchCategoryForMen = createAsyncThunk(
    'category/fetchCategoryForMen',
    async () => {
        const data = await sendRequest(API_URL_CATEGORY_MEN)
        return data
    }
)

export const actionFetchCategoryMenItem = createAsyncThunk(
    'categoryForMen/cardItem',
    async (path) => {
        const data = await sendRequest(`../../public/${path}.json`)
        // console.log(data);
        return data
    }
)


const categoryForMenSlice = createSlice({
    name: 'categoryForMen',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(actionFetchCategoryForMen.fulfilled, (state, { payload }) => {
                state.listCategoryMen = [...payload]
            })
            .addCase(actionFetchCategoryMenItem.fulfilled, (state, action) => {
                const numberLink = action.meta.arg.slice(action.meta.arg.lastIndexOf('/') + 1)
                state.listCategoryForMenCards[numberLink] = action.payload
            })

    }
})



export default categoryForMenSlice.reducer