import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'


const initialState = {
    listFavorite: [],
}



const favoriteSlice = createSlice({
    name: 'favorite',
    initialState,
    reducers: {
        actionAddFavoriteList: (state, { payload }) => {
            if (state.listFavorite.some((item) => item.article === payload.article)) {
                state.listFavorite = [...state.listFavorite].filter((item) => item.article !== payload.article)
                localStorage.setItem('listFavorite', JSON.stringify(state.listFavorite))

            } else {
                state.listFavorite = [...state.listFavorite, payload]
                localStorage.setItem('listFavorite', JSON.stringify(state.listFavorite))
            }

        },
        actiohSetListFavoriteFromLocalSrorage: (state, action) => {
            const listFavoriteFromLS = localStorage.getItem('listFavorite')
            if (listFavoriteFromLS) {
                state.listFavorite = [...JSON.parse(listFavoriteFromLS)]
            }
        }
    }
})

export const {
    actionAddFavoriteList,
    actiohSetListFavoriteFromLocalSrorage,
} = favoriteSlice.actions

export default favoriteSlice.reducer


