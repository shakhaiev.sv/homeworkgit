import { createSlice } from '@reduxjs/toolkit'


const initialState = {
    listBasket: [],
    isOpenModalAddBasket: false,
    isOpenModalRemoveBasket: false,
    descriptionModal: {}
}

const BasketSlice = createSlice({
    name: 'basket',
    initialState,
    reducers: {
        actionAddBasketList: (state, { payload }) => {
            state.listBasket = [...state.listBasket, payload]
            localStorage.setItem('listBasket', JSON.stringify(state.listBasket))
        },
        actiohSetListBasketFromLocalSrorage: (state, action) => {
            const listBasketFromLS = localStorage.getItem('listBasket')
            if (listBasketFromLS) {
                state.listBasket = [...JSON.parse(listBasketFromLS)]
            }
        },
        actionIsOpenModalAddBasket: (state, action) => {
            state.isOpenModalAddBasket = !state.isOpenModalAddBasket
        },
        actionCreatedModal: (state, { payload }) => {
            state.descriptionModal = { ...payload }
        },
        actionIsOpenModalRemoveBasket: (state, action) => {
            state.isOpenModalRemoveBasket = !state.isOpenModalRemoveBasket
        },
        actionRemoveCardFromBasketList: (state, { payload }) => {
            state.listBasket = [...state.listBasket].filter((item) => item.article !== payload.article)
            localStorage.setItem('listBasket', JSON.stringify(state.listBasket))
        }
    },
})

export const {
    actionAddBasketList,
    actiohSetListBasketFromLocalSrorage,
    actionIsOpenModalAddBasket,
    actionIsOpenModalRemoveBasket,
    actionCreatedModal,
    actionRemoveCardFromBasketList
} = BasketSlice.actions

export default BasketSlice.reducer