

export const sendRequest = async (url) => {
    const responce = await fetch(url);
    const result = await responce.json()
    return result
}