export const createTitlePage = (title) => {
    try {
        if (title.includes('&')) {
            const arrayTitle = title.split('&')
            return `${arrayTitle[0][0].toLocaleUpperCase()}${arrayTitle[0].slice(1)} & ${arrayTitle[1][0].toLocaleUpperCase()}${arrayTitle[1].slice(1)}`
        }

        return `${title[0].toLocaleUpperCase() + title.slice(1)}`
    } catch (e) {
        console.log(e.message);
    }

}