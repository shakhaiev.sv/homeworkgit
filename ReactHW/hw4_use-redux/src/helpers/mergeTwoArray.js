
export const mergeTwoArray = (checkerStr, arrBasket, arrFavorite) => {
    try {
        if (Array.isArray(arrFavorite) && Array.isArray(arrBasket)) {
            if (arrFavorite.length === 0 && arrBasket.length === 0 && typeof checkerStr !== 'string') {
                return []
            }
            if (checkerStr === 'basket') {

                const result = arrBasket.map((item) => {
                    return updateObject(item, arrFavorite)
                })
                return result
            }
            if (checkerStr === 'favorite') {
                const result = arrFavorite.map((item) => {
                    return updateObject(item, arrBasket)
                })
                return result
            }
        }
    } catch (error) {
        console.log(e.message);
    }
}


function updateObject(obj, array) {
    try {
        const { article, nameID } = obj
        const card = array.find((item) => item['article'] === article && item['nameID'] === nameID)
        if (card) {
            return {
                ...obj,
                ...card
            }
        } else {
            return obj
        }

    } catch (error) {
        console.log(error);
    }
}