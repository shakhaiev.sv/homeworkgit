export function mergeList(arrFetch, arrFavorite, arrBasket) {

    try {
        if (Array.isArray(arrFetch) && Array.isArray(arrFavorite) && Array.isArray(arrBasket)) {
            if (arrFetch.length === 0) {
                return []
            }

            if (arrFavorite.length === 0 && arrBasket.length === 0) {
                return arrFetch
            }

            const arrayFavoriteArticle = [...arrFavorite].filter((item) => {

                if (item['nameID'] === arrFetch[0]['nameID']) {
                    return (item)
                }
            }).map(item => item.article)

            const arrayBasketArticle = [...arrBasket].filter((item) => {

                if (item['nameID'] === arrFetch[0]['nameID']) {
                    return (item)
                }
            }).map(item => item.article)

            const arrayCurrentArticle = [...new Set([...arrayFavoriteArticle, ...arrayBasketArticle])]


            let result = arrFetch.map((item) => {

                if (arrayCurrentArticle.includes(item.article)) {

                    const objUpdateFavorite = arrFavorite.find((favorite) => {
                        return favorite.article === item.article

                    })
                    const objUpdateBasket = arrBasket.find((basket) => {
                        return basket.article === item.article

                    })

                    return {
                        ...item,
                        ...objUpdateFavorite,
                        ...objUpdateBasket
                    }
                } else {

                    return item
                }
            })
            return result
        }

    } catch (e) {
        console.log(e.message);

    }
}