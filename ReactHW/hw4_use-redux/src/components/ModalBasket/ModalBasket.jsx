import './ModalBasket.scss'
import Modal from "../Modal/Modal.jsx"
import Button from "../Button/Button.jsx"
import PropTypes from 'prop-types'
import { actionAddBasketList } from '../../store/slices/basket.js'
import { useDispatch } from 'react-redux'


const ModalCard = (props) => {
    const {
        onClose,
        currentCard,
    } = props
    const dispatch = useDispatch()


    const { urlImg, name: title, price, brand, article, nameID, color } = currentCard

    const handleAddBasket = (card) => {
        dispatch(actionAddBasketList(card))
    }

    return (
        <Modal
            onClose={onClose}
            currentCard={currentCard}
        >
            <div className="modal-card">
                <div className="modal-info">
                    <div className='modal-img'>
                        <img src={urlImg} alt={title} />
                    </div>
                    <div className="modal-desc">
                        <h1 className="modal-title">{title}</h1>
                        <p className="modal-brand">{brand}</p>
                        <p className="modal-price">${price}</p>
                        <p className="modal-text">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusamus ea laudantium saepe praesentium asperiores error quidem dolor dolore numquam esse voluptatibus ipsam fuga veniam pariatur, suscipit laborum quia fugit non!</p>

                    </div>
                </div>

                <div className="modal-wrap-btn">
                    <Button
                        onClick={() => {
                            const btnBasket = document.querySelector(`.card[data-article="${article}"] .btn-goods[data-dbbasket]`)
                            btnBasket.dataset.dbbasket = true
                            handleAddBasket({
                                'nameID': nameID,
                                'urlImg': urlImg,
                                'name': title,
                                "price": price,
                                "brand": brand,
                                'article': article,
                                'color': color,
                                'dbAttrBasket': btnBasket.dataset.dbbasket
                            })
                            onClose()
                        }}
                        className='modal-btn'
                    >
                        Add to basket
                    </Button>
                </div>
            </div>
        </Modal >
    )
}

ModalCard.propTypes = {
    onClose: PropTypes.func,
    currentCard: PropTypes.object,
}

export default ModalCard