import Button from "../Button/Button"
import SearchSvg from './images/svg/Search.svg?react'


const Search = () => {

    return (
        <Button
            className="header-search"
            type="button"
        >
            <SearchSvg />
            <span>Search</span>
        </Button>
    )
}

export default Search