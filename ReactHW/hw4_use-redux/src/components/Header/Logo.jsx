
import { Link } from 'react-router-dom'

const Logo = () => {

    return (
        <Link to='/' className="header-logo">
            <img src="../public/images/header/logo.png" className="header-img" alt="euphoria" />
        </Link>
    )
}

export default Logo