import Button from "../Button/Button"
import BasketIcon from './images/svg/basket.svg?react'
import cn from 'classnames'
import PropTypes from 'prop-types'


const Basket = (props) => {
    const { cart, onClick, dbbasket, to } = props
    return (
        <Button
            data-dbbasket={dbbasket}
            className={cn('btn-goods', 'active', { "count-goods": cart })}
            data-cart={cart}
            onClick={onClick}
            to={to}
        >
            <BasketIcon />
        </Button>
    )
}

Basket.propTypes = {
    cart: PropTypes.number,
    onClick: PropTypes.func,
    to: PropTypes.string,
    dbbasket: PropTypes.bool
}

export default Basket