import "./Header.scss"
import Logo from './Logo.jsx'
import Menu from './Menu.jsx'
import Search from "./Search.jsx"
import Basket from "./Basket.jsx"
import Favorite from "./Favorite.jsx"
import { selectorListFavorite, selectorListBasket } from '../../store/selectors/selectors.js'
import { useSelector } from "react-redux"

const Navigation = () => {

    const listFavorite = useSelector(selectorListFavorite)
    const listBasket = useSelector(selectorListBasket)
    return (
        <nav className="header-nav">
            <Logo />
            <Menu />
            <Search />
            <div className="header-goods">
                <Basket
                    to='/basket'
                    cart={listBasket.length} />

                <Favorite
                    to='/favorite'
                    favorite={listFavorite.length} />
            </div>

        </nav>
    )
}

export default Navigation