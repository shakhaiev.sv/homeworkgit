import React from 'react';
import './ModalBasketRemove.scss'
import Modal from '../Modal/Modal.jsx';
import Button from '../Button/Button.jsx';
import { actionRemoveCardFromBasketList } from '../../store/slices/basket.js'
import { useDispatch } from 'react-redux'
import PropTypes from 'prop-types'

const ModalBasketRemove = (props) => {
    const { onClose, currentCard } = props

    const dispatch = useDispatch()

    const handleRemoveCardfromBasket = (card) => {
        dispatch(actionRemoveCardFromBasketList(card))
    }
    return (
        <Modal
            onClose={onClose}
        >
            <div className='modal-remove'>
                <p className='modal-remove__desc'>
                    Do you really want to remove the item from the cart?
                </p>
                <Button
                    onClick={() => {
                        handleRemoveCardfromBasket(currentCard)
                        onClose()
                    }}
                    className='modal-btn'
                >
                    Yes, remove
                </Button>
            </div>
        </Modal>
    );
};

ModalBasketRemove.propTypes = {
    onClose: PropTypes.func,
    currentCard: PropTypes.object,
};

export default ModalBasketRemove;