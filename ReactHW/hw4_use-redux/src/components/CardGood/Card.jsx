import './Card.scss'
import Delete from "./images/svg/Delete.svg?react"
import Favorite from '../Header/Favorite.jsx'
import Basket from '../Header/Basket.jsx'
import PropTypes from 'prop-types'
import Button from '../Button/Button.jsx'
import { useDispatch } from 'react-redux'
import { actionAddFavoriteList } from '../../store/slices/favorite.js'
import { actionIsOpenModalAddBasket, actionIsOpenModalRemoveBasket, actionCreatedModal } from '../../store/slices/basket.js'



const Card = (props) => {
    const { product, hideBtnBasket, hideBtnDelete } = props;

    const dispatch = useDispatch()

    const { name: title, article, urlImg, brand, price, dbAttrFavorite, dbAttrBasket, color } = product;

    const handleFavorite = (e) => {
        let elementFavoriteBtn = e.target.closest('.btn')

        elementFavoriteBtn.dataset.dbfavorite = !elementFavoriteBtn.dataset.dbfavorite
        dispatch(actionAddFavoriteList({
            ...product,
            'dbAttrFavorite': true
        }))
    }

    const handleAddBasket = (e) => {
        dispatch(actionIsOpenModalAddBasket())
        dispatch(actionCreatedModal({
            ...product,

        }))
    }

    const handleRemoveCardFromBasket = () => {
        dispatch(actionIsOpenModalRemoveBasket())
        dispatch(actionCreatedModal({
            ...product
        }))
    }

    return (
        <li
            data-article={article}
            className='card'
        >
            <img className='item-img' src={urlImg} alt="img" />

            <div className="card-desc">
                <div className='wraper'>
                    <h3 className='title'>{title}</h3>
                    <p className='brand'>{brand}</p>
                </div>
                <a
                    className='price'
                    href="#">${price}
                </a>
            </div>
            <div
                className='card-add-favorite'>
                <div className="color">
                    {color.map((item, index) => <a href="#" key={index} style={{
                        backgroundColor: item,
                        width: '15px',
                        height: '15px',
                        borderRadius: "50%",
                        display: 'block'
                    }}></a>)}

                </div>
                <div className='btn-wrap'>
                    <Favorite
                        dbfavorite={dbAttrFavorite === undefined ? false : true}
                        onClick={handleFavorite}
                    />
                    {!hideBtnBasket && <Basket
                        dbbasket={dbAttrBasket === undefined ? false : true}
                        onClick={handleAddBasket}
                    />}
                    {!hideBtnDelete && <Button
                        onClick={handleRemoveCardFromBasket}
                        className='btn-goods'>
                        <Delete />
                    </Button>}
                </div>
            </div>
        </li>
    )
}


Card.propTypes = {
    product: PropTypes.object,
    hideBtnBasket: PropTypes.bool,
    hideBtnDelete: PropTypes.bool
}

export default Card