import './Limelight.scss'
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Container from '../../layout/Container/Container'
import ModalBasket from '../../../components/ModalBasket/ModalBasket.jsx'
import ModalBasketRemove from '../../../components/ModalBasketRemove/ModalBasketRemove.jsx'
import Card from '../../../components/CardGood/Card.jsx'


import {
    selectorlistLimeLight,
    selectorListFavorite,
    selectorListBasket,
    selectorIsOpenModalRemoveBasket,
    selectorIsOpenModalAddBasket,
    selectorDescriptionModal
} from '../../../store/selectors/selectors.js'

import { actionFetchListLimelight } from '../../../store/slices/limelight.js'
import { actionIsOpenModalRemoveBasket, actionIsOpenModalAddBasket } from '../../../store/slices/basket.js'

import { mergeList } from '../../../helpers/mergeList.js'


const Limelight = () => {
    const [listCard, setListCard] = useState([])
    const dispatch = useDispatch()
    const listLimelight = useSelector(selectorlistLimeLight)
    const listFavorite = useSelector(selectorListFavorite)
    const listBasket = useSelector(selectorListBasket)
    const isModalAddBasket = useSelector(selectorIsOpenModalAddBasket)
    const isModalRemoveBasket = useSelector(selectorIsOpenModalRemoveBasket)
    const currentCard = useSelector(selectorDescriptionModal)

    const handleCloseModalAddBasket = () => { dispatch(actionIsOpenModalAddBasket()) }
    const handleCloseModalRemoveBasket = () => { dispatch(actionIsOpenModalRemoveBasket()) }

    useEffect(() => {
        dispatch(actionFetchListLimelight())
    }, [])

    useEffect(() => {

        const listCurrent = mergeList(listLimelight, listFavorite, listBasket)
        setListCard(listCurrent || [])
    }, [listFavorite, listBasket, listLimelight])

    return (
        <section className="sect-limelight">
            <Container>
                <h1 className="sect-title sect-limelight__title">In The Limelight</h1>
                <ul className='sect-limelight__list list-card'>
                    {listCard.map((item, index) => {
                        return <Card
                            key={index}
                            product={item}
                        />
                    })}
                </ul>
            </Container>
            {isModalAddBasket && <ModalBasket
                onClose={handleCloseModalAddBasket}
                currentCard={currentCard}
            />}
            {isModalRemoveBasket && <ModalBasketRemove
                onClose={handleCloseModalRemoveBasket}
                currentCard={currentCard}
            />}
        </section>
    );
}


export default Limelight