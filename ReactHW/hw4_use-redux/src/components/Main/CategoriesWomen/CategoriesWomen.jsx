
import './CategoriesWomen.scss'
import Container from '../../layout/Container/Container'
import CardCategories from '../../CardCategories/CardCategories.jsx'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectorCategoryForWomen } from '../../../store/selectors/selectors.js'
import { actionFetchCategoryForWomen } from '../../../store/slices/categoryWomen.js'

const CategoriesWomen = () => {
    const dispatch = useDispatch()
    const listCategoryWomen = useSelector(selectorCategoryForWomen)

    useEffect(() => {
        dispatch(actionFetchCategoryForWomen())
    }, [])

    return (
        <section className="sect-categ-women">
            <Container>
                <h1 className="sect-title sect-categ-women__title">Categories For Women</h1>
                <ul className='list-card categor-men-list'>
                    {listCategoryWomen.map((item, index) => {
                        return <CardCategories
                            key={index}
                            card={item}
                            path='categoryforWomen'
                        />
                    }
                    )}
                </ul>
            </Container>
        </section>
    )
}

export default CategoriesWomen