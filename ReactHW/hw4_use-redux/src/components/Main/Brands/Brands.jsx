
import './Brands.scss'
import Container from '../../layout/Container/Container'


const Brands = () => {

    return (
        <section className="sect-brands">
            <Container>
                <div className="sect-brands__wrap">
                    <h1 className="sect-brands__title">Top Brands Deal</h1>
                    <p className="sect-brands__text">Up To <span>60%</span> off on brands</p>
                    <ul className="sect-brands__list">
                        <li className="sect-brands__item">
                            <img className="sect-brands__img" src="./public/images/brands/nike.png" alt="nike" />
                        </li>
                        <li className="sect-brands__item">
                            <img className="sect-brands__img" src="./public/images/brands/hm.png" alt="h&m" />
                        </li>
                        <li className="sect-brands__item">
                            <img className="sect-brands__img" src="./public/images/brands/levis.png" alt="levis" />
                        </li>
                        <li className="sect-brands__item">
                            <img className="sect-brands__img" src="./public/images/brands/polo.png" alt="polo" />
                        </li>
                        <li className="sect-brands__item">
                            <img className="sect-brands__img" src="./public/images/brands/puma.png" alt="puma" />
                        </li>
                    </ul>
                </div>
            </Container>
        </section>
    )
}

export default Brands