import { Routes, Route } from "react-router-dom";

import Main from '../components/Main/Main.jsx'
import NotPage from '../pages/notPages/NotPage.jsx'
import FavoritePage from "../pages/FavoritePage/FavoritePage.jsx";
import BasketPage from '../pages/BasketPage/BasketPage.jsx'
import Limelight from "../components/Main/Limelight/Limelight.jsx";
import ItemCategoryMenCards from '../pages/ItemCategoryMenCards/ItemCategoryMenCards.jsx'
import ItemCategoryWonenCards from '../pages/ItemCategoryWomenCards/ItemCategoryWomenCards.jsx'



export default () => {
    return (
        <Routes>
            <Route path="/" element={<Main />} />
            <Route path={'/categoryforMen/:id'} element={<ItemCategoryMenCards />} />
            <Route path={'/categoryforWomen/:id'} element={<ItemCategoryWonenCards />} />
            <Route path='/favorite' element={<FavoritePage />} />
            <Route path='/basket' element={<BasketPage />} />
            <Route path='/limelight' element={<Limelight />} />
            <Route path="*" element={<NotPage />} />

        </Routes>
    )
}