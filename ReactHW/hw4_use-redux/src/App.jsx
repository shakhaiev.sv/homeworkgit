
import { useEffect, useState } from 'react'
import './App.scss'
import Footer from './components/Footer/Footer.jsx'
import Header from './components/Header/Header.jsx'
import AppRoute from './routers/index.jsx'
import { useDispatch } from 'react-redux'
import { actiohSetListFavoriteFromLocalSrorage } from './store/slices/favorite.js'
import { actiohSetListBasketFromLocalSrorage } from './store/slices/basket.js'

function App() {

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(actiohSetListFavoriteFromLocalSrorage())
    dispatch(actiohSetListBasketFromLocalSrorage())
  }, [])

  return (
    <>
      <Header />
      <main className='page-main'>
        <AppRoute />
      </main>
      <Footer />
    </>
  )
}

export default App
