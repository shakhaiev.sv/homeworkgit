
import './CategoriesMen.scss'
import Container from '../../layout/Container/Container'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectorCategoryForMen } from '../../../store/selectors/selectors.js'
import { actionFetchCategoryForMen } from '../../../store/slices/categoryMen.js'
import CardCategories from '../../CardCategories/CardCategories.jsx'

const CategoriesMen = () => {
    const dispatch = useDispatch()
    const listCategoryMen = useSelector(selectorCategoryForMen)

    useEffect(() => {
        dispatch(actionFetchCategoryForMen())
    }, [])

    return (
        <section className="sect-categ-men">
            <Container>
                <h1 className="sect-title sect-categ-men__title">Categories For Men</h1>

                <ul className='list-card categor-men-list'>
                    {listCategoryMen.map((item, index) => {
                        return <CardCategories
                            key={index}
                            card={item}
                            path='categoryforMen'
                        />
                    }
                    )}
                </ul>
            </Container>
        </section>
    )
}

export default CategoriesMen