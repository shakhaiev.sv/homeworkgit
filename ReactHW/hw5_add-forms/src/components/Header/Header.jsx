import Navigation from "./Navigation"
import Container from "../layout/Container/Container"
import PropTypes from 'prop-types'


const Header = () => {
    return (
        <header className="header">
            <Container>
                <Navigation />
            </Container>
        </header>
    )
}

export default Header