import "./Header.scss"
import Logo from './Logo.jsx'
import Menu from './Menu.jsx'
import Search from "./Search.jsx"
import Basket from "./Basket.jsx"
import Favorite from "./Favorite.jsx"
import { selectorListFavorite, selectorListBasket, selectorAllLengthGoodsOnBasket } from '../../store/selectors/selectors.js'

import { actionAllLengthCoodsOnBasket } from '../../store/slices/basket.js'
import { useSelector, useDispatch } from "react-redux"
import { useEffect } from 'react'

const Navigation = () => {

    const listFavorite = useSelector(selectorListFavorite)
    const listBasket = useSelector(selectorAllLengthGoodsOnBasket)


    return (
        <nav className="header-nav">
            <Logo />
            <Menu />
            <Search />
            <div className="header-goods">
                <Favorite
                    to='/favorite'
                    favorite={listFavorite.length} />
                <Basket
                    to='/basket'
                    cart={listBasket} />


            </div>

        </nav>
    )
}

export default Navigation