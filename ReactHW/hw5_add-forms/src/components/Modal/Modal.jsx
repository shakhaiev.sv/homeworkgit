import { ModalBody } from "./ModalBody"
import { ModalClose } from "./ModalClose"
import { ModalWrapper } from "./ModalWrapper"
import './Modal.scss'
import PropTypes from 'prop-types'


const Modal = (props) => {
    const { onClose, currentCard, children } = props
    return (
        <ModalWrapper onClose={onClose}>
            <div className="modal">
                <ModalClose onClose={onClose} />
                <ModalBody
                    currentCard={currentCard}>
                    {children}
                </ModalBody>
            </div>

        </ModalWrapper>
    )
}

Modal.propTypes = {
    onClose: PropTypes.func,
    currentCard: PropTypes.object
}

export default Modal