import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    dataDelivery: {
        firstName: '',
        lastName: '',
        age: '',
        phone: '',
        city: '',
        address: '',
        email: '',
        saveInfo: false,
    },

    order: {}
}

const deliverySlice = createSlice({
    name: 'delivery',
    initialState,
    reducers: {
        actionDataDelivery: (state, { payload }) => {
            state.order = payload
            localStorage.removeItem('listBasket')
            const { dataClient, listBasket } = payload

            console.group()
            console.log(`First Name : ${dataClient.firstName}`);
            console.log(`Last Name : ${dataClient.lastName}`);
            console.log(`Age: ${dataClient.age}`);
            console.log(`Phone : ${dataClient.phone}`);
            console.log(`City : ${dataClient.city}`);
            console.log(`Street Address : ${dataClient.address}`);
            console.log(`Email : ${dataClient.email}`);
            console.groupEnd()
            console.log(' ');

            listBasket.forEach((item) => {
                console.group()
                console.log(`Name : ${item.name}`);
                console.log(`Brand : ${item.brand}`);
                console.log(`Price: ${item.price}`);
                console.log(`Article : ${item.article}`);
                console.log(`Color : ${item.color[0]}`);
                console.groupEnd()
                console.log(' ');
            })
        }
    }
})

export const { actionDataDelivery } = deliverySlice.actions

export default deliverySlice.reducer