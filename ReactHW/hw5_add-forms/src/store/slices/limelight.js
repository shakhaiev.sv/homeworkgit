import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { sendRequest } from "../../helpers/sendRequest";
import { API_URL_CATEGORY_LIMELIGHT } from '../../constans/API.js'

const initialState = {
    listLimeLight: [],
}


export const actionFetchListLimelight = createAsyncThunk(
    'category/fetchCategoryForLimelight',
    async () => {
        const data = await sendRequest(API_URL_CATEGORY_LIMELIGHT)
        return data
    }
)

const limelightSlice = createSlice({
    name: 'categoryForWomen',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(actionFetchListLimelight.fulfilled, (state, { payload }) => {
                state.listLimeLight = [...payload]
            })
    }
})

export default limelightSlice.reducer