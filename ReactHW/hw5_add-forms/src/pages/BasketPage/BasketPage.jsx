
import './BasketPage.scss'
import Container from '../../components/layout/Container/Container.jsx'
import { Link } from "react-router-dom"
import {
    selectorListBasket,
    selectorListFavorite,
    selectorAllSummGoodsOnBasket,
    selectorAllLengthGoodsOnBasket,
    selectorIsOpenModalRemoveBasket,
    selectorDescriptionModal,
    selectorSavings,
    selectorShipping,

} from '../../store/selectors/selectors.js'
import { useSelector, useDispatch } from 'react-redux'
import {
    actionIsOpenModalRemoveBasket,
    actionAllSummGoodsOnBasket,
    actionRandomSavings,
    actionRandomShipping,
} from '../../store/slices/basket.js'
import ModalBasketRemove from '../../components/ModalBasketRemove/ModalBasketRemove.jsx'
import FormBasket from './FormBasket/FormBasket.jsx'
import CardOrder from './CardOrder/CardOrder.jsx'
import { useEffect } from 'react'


const BasketPage = () => {

    const dispatch = useDispatch()

    const listBacket = useSelector(selectorListBasket)
    const listFavorite = useSelector(selectorListFavorite)
    const allLengthGoodsOnBasket = useSelector(selectorAllLengthGoodsOnBasket)
    const allSummhGoodsOnBasket = useSelector(selectorAllSummGoodsOnBasket)
    const isModalRemoveBasket = useSelector(selectorIsOpenModalRemoveBasket)
    const savings = useSelector(selectorSavings)
    const shipping = useSelector(selectorShipping)

    const currentCard = useSelector(selectorDescriptionModal)
    const handleCloseModalRemoveBasket = () => { dispatch(actionIsOpenModalRemoveBasket()) }

    useEffect(() => {
        dispatch(actionAllSummGoodsOnBasket())
        dispatch(actionRandomSavings())
        dispatch(actionRandomShipping())
    }, [])


    return (
        <section className="sect-basket">
            <Container>
                {!listBacket.length ? <div className="list-basket-empty">
                    <p className="list-basket-empty__text">
                        Oops! Items are missing, check out our <Link to='/limelight' >best items</Link>
                    </p>
                </div> :
                    <>
                        <h1 className="sect-title  sect-basket__title">Ckeck Out </h1>

                        <div className='basket-wrapper'>
                            <FormBasket />

                            <div className='basket-order'>
                                <h3 className='basket-order__title'>Order Summary</h3>
                                <ul className="basket-order__list">
                                    {listBacket.map((item, index) => {
                                        return <CardOrder
                                            key={index}
                                            product={item}
                                            showBtn='true'
                                            favorite={listFavorite.some((product) => product.nameID === item.nameID && product.article === item.article)}
                                        />
                                    })}
                                </ul>
                                <div className='basket-order__subtotal dfjcsbaic line'>
                                    <p className='basket-order__subtotal-title   fs '>
                                        Subtotal
                                        <span className='basket-order__quantity-goods'>{` ( ${allLengthGoodsOnBasket} items )`}</span>
                                    </p>
                                    <p className='basket-order__subtotal-price fs'>{`$${allSummhGoodsOnBasket}`}</p>
                                </div>
                                <div className='basket-order__savings dfjcsbaic '>
                                    <p className='basket-order__savings-title fs'>Savings</p>
                                    <p className='basket-order__savings-price fs'>{`-$${savings}`}</p>
                                </div>

                                <div className='basket-order__shipping dfjcsbaic line'>
                                    <p className='basket-order__savings-title fs '>Shipping</p>
                                    <p className='basket-order__savings-price fs'>{`-$${shipping}`}</p>
                                </div>

                                <div className='basket-order__total dfjcsbaic line'>
                                    <p className='basket-order__total-title fs '>Total</p>
                                    <p className='basket-order__total-price fs'>{`$${allSummhGoodsOnBasket - savings - shipping}`}</p>
                                </div>

                            </div>

                        </div>
                    </>
                }
            </Container>
            {isModalRemoveBasket && <ModalBasketRemove
                onClose={handleCloseModalRemoveBasket}
                currentCard={currentCard} />}
        </section>
    )
}


export default BasketPage