import *as yup from 'yup'

const validate = yup.object({
    firstName: yup
        .string()
        .min(3, 'name is to shoort')
        .max(25, 'name is to long')
        .required(' '),
    lastName: yup
        .string()
        .min(3, 'name is to shoort')
        .max(25, 'name is to long')
        .required(' '),
    age: yup
        .number('')
        .min(18, 'is not 18 years old')
        .required(' '),
    phone: yup
        .number(' ')
        .required(' '),
    city: yup
        .string()
        .min(2, ' ')
        .required(' '),
    address: yup
        .string()
        .min(2, ' ')
        .required(' '),
    email: yup
        .string()
        .email("not email")
        .matches(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, "non valid mail")
        .required(" "),
}).shape({
    saveInfo: yup
        .bool()
        .oneOf([true], 'You need to accept the terms and conditions')
})

export default validate 