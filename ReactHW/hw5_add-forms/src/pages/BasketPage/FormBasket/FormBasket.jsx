import React from 'react';
import './FormBasket.scss'
import { Formik, Form } from 'formik';
import Input from '../../../components/Form/Input/Input.jsx'
import { useSelector, useDispatch } from 'react-redux';
import { actionDataDelivery } from '../../../store/slices/delivery.js'
import { actionDeleteGoodsOnBasket, actionAllLengthCoodsOnBasket } from '../../../store/slices/basket.js'
import Button from '../../../components/Button/Button.jsx';
import { selectorDelivery, selectorListBasket } from '../../../store/selectors/selectors.js';

import validation from './validate.js'
import Checxbox from '../../../components/Form/Checkbox/Checxbox.jsx';
import { useNavigate } from 'react-router-dom';



const FormBasket = () => {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const dataDevilery = useSelector(selectorDelivery)
    const listBasket = useSelector(selectorListBasket)

    return (
        <Formik
            initialValues={dataDevilery}
            validationSchema={validation}
            onSubmit={(values, { resetForm }) => {
                dispatch(actionDataDelivery({ 'dataClient': values, 'listBasket': listBasket }))
                dispatch(actionDeleteGoodsOnBasket())
                dispatch(actionAllLengthCoodsOnBasket())
                navigate('/order')
                resetForm()
            }}
        >
            {({ errors, touched, isSubmitting }) => (
                <Form className='form-basket' >
                    <h3 className='form-basket-title'>Billing Details</h3>
                    <div className='row'>
                        <Input
                            type='text'
                            placeholder='First Name'
                            label="First Name*"
                            name='firstName'
                            error={errors.firstName && touched.firstName}
                        />
                        <Input
                            type='text'
                            placeholder='Last Name'
                            label="Last Name*"
                            name='lastName'
                            error={errors.lastName && touched.lastName}
                        />
                    </div>
                    <div className='row'>
                        <Input
                            type='number'
                            placeholder='Age'
                            label="Age"
                            name='age'
                            error={errors.age && touched.age}
                        />
                        <Input
                            type='number'
                            placeholder='Phone'
                            label="Phone*"
                            name='phone'
                            format='+38(###) ### ## ##'
                            error={errors.phone && touched.phone}
                        />
                    </div>
                    <div className='row'>
                        <Input
                            type='text'
                            placeholder='Town / City'
                            label="City*"
                            name='city'
                            error={errors.city && touched.city}
                        />
                        <Input
                            type='text'
                            placeholder='Street Address'
                            label="Street Address*"
                            name='address'
                            error={errors.address && touched.address}
                        />
                    </div>
                    <div className='row'>
                        <Input
                            type='text'
                            placeholder='Email'
                            label="Email*"
                            name='email'
                            error={errors.email && touched.email}
                        />
                    </div>
                    <div className='row'>
                        <div className='col'>
                            <Checxbox
                                classLabel='form-checkbox'
                                type='checkbox'
                                name='saveInfo'
                                label='Save my information for a faster checkout'
                                error={errors.saveInfo && errors.touched}
                            />
                        </div>
                    </div>

                    <div>
                        <Button
                            type='submit'
                            className='btn-submit'
                        >Continue to delivery</Button>
                    </div>
                </Form>
            )}

        </Formik>
    );
};

FormBasket.propTypes = {};

export default FormBasket;