import { useSelector, useDispatch } from 'react-redux'
import Card from "../../components/CardGood/Card"
import Container from "../../components/layout/Container/Container"
import ModalBasket from '../../components/ModalBasket/ModalBasket.jsx'

import {
    selectorListFavorite,
    selectorListBasket,
    selectorDescriptionModal,
    selectorIsOpenModalAddBasket
} from '../../store/selectors/selectors.js'

import { actionIsOpenModalAddBasket } from '../../store/slices/basket.js'
import './FavoritePage.scss'
import { Link } from "react-router-dom"



const FavoritePage = () => {
    const dispatch = useDispatch()
    const ListFavorite = useSelector(selectorListFavorite)
    const listBacket = useSelector(selectorListBasket)
    const isModalAddBasket = useSelector(selectorIsOpenModalAddBasket)
    const currentCard = useSelector(selectorDescriptionModal)


    const handleCloseModalAddBasket = () => { dispatch(actionIsOpenModalAddBasket()) }

    return (
        <section className="sect-favorite">
            <Container>
                <h1 className="sect-title  sect-favorite__title">My basket products </h1>
                <ul className="list-favorite">
                    {ListFavorite.map((item, index) => {
                        return <Card
                            key={index}
                            product={item}
                            hideBtnDelete={true}
                            favorite={true}
                            basket={listBacket.some((product) => product.nameID === item.nameID && product.article === item.article)}

                        />
                    })}
                </ul>
                {!ListFavorite.length && <div className="list-favorite-empty">
                    <p className="list-favorite-empty__text">
                        Oops! Items are missing, check out our <Link to='/limelight' >best items</Link>
                    </p>
                </div>}
            </Container>
            {isModalAddBasket && <ModalBasket
                onClose={handleCloseModalAddBasket}
                currentCard={currentCard}
            />}
        </section>


    )
}

export default FavoritePage