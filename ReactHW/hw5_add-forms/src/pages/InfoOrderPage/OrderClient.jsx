import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { selectorOrderClient } from '../../store/selectors/selectors.js'
import Container from '../../components/layout/Container/Container';
import CardOrder from '../BasketPage/CardOrder/CardOrder.jsx';
import './OrderClient.scss'


const OrderClient = () => {

    const orderClient = useSelector(selectorOrderClient)


    const { lastName = '', firstName = '', email = '', city = '', age = '', address = '', phone = '' } = orderClient?.dataClient



    return (
        <section className="sect-order">
            < Container >
                <h1 className="sect-title sect-order__title">Your Order</h1>
                <div className='sect-order__wrapper'>
                    <div className='sect-order__client-info'>
                        <p className='sect-order__name'>First Name : <span className='sect-order__value'>{firstName}</span> </p>
                        <p className='sect-order__name'>Last Name : <span className='sect-order__value'>{lastName}</span> </p>
                        <p className='sect-order__name'>Age : <span className='sect-order__value'>{age}</span> </p>
                        <p className='sect-order__name'>Phone : <span className='sect-order__value'>{phone}</span> </p>
                        <p className='sect-order__name'>City : <span className='sect-order__value'>{city}</span> </p>
                        <p className='sect-order__name'>Street Address : <span className='sect-order__value'>{address}</span> </p>
                        <p className='sect-order__name'>Email : <span className='sect-order__value'>{email}</span> </p>

                    </div>
                    <div className='sect-order__buy-goods'>
                        {orderClient.listBasket.map((item, index) => {
                            return <CardOrder
                                key={index}
                                product={item}
                            />
                        })}
                    </div>
                </div>
            </Container >
        </section >



    );
};

OrderClient.propTypes = {};

export default OrderClient;