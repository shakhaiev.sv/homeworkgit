import { useState } from "react"

import Card from "../../components/CardGood/Card"
import Container from "../../components/layout/Container/Container"
import ModalFavorite from "./ModalFavorite/ModalFavorite"

import './FavoritePage.scss'
import { Link } from "react-router-dom"




const FavoritePage = (props) => {
    const { favorite, onFavorite, cart, onCart } = props

    const [isModal, setIsModal] = useState(false)

    const handleModalCard = () => {
        setIsModal(!isModal)
    }

    const [currentCard, setCurrentCard] = useState({})
    const handleCurrentCard = (card) => {
        setCurrentCard(card)
    }


    return (
        <section className="sect-favorite">
            <Container>

                <h1 className="sect-title  sect-favorite__title">My favorite products </h1>
                <ul className="list-favorite">
                    {favorite.map((item, index) => {
                        return <Card
                            favorite={favorite}
                            key={index}
                            product={item}
                            showModalCard={handleModalCard}
                            handleCurrentCard={handleCurrentCard}
                            deleteFavorite='true'
                            cart={cart}
                            onCart={onCart}
                        />
                    })}
                </ul>
                {!favorite.length && <div className="list-favorite-empty">
                    <p className="list-favorite-empty__text">
                        Oops! Items are missing, check out our <Link to='/limelight' >best items</Link>

                    </p>

                </div>}
            </Container>

            {isModal && <ModalFavorite
                onClose={handleModalCard}
                currentCard={currentCard}
                onFavorite={onFavorite}
            />}
        </section>


    )
}

export default FavoritePage