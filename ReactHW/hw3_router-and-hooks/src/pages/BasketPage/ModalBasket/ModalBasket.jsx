import Modal from "../../../components/Modal/Modal";
import Button from "../../../components/Button/Button";

import './ModalBasket.scss'


const ModalBasket = (props) => {

    const { onClose, currentCard, onCart } = props

    const { urlImg, title, price, brand, article, } = currentCard
    return (
        <Modal
            onClose={onClose}
            currentCard={currentCard}
        >
            <div className="modal-basket">
                <div className="modal-info">
                    <div className='modal-img'>
                        <img src={urlImg} alt={title} />
                    </div>
                    <div className="modal-desc">
                        <h1 className="modal-title">{title}</h1>
                        <p className="modal-brand">{brand}</p>
                        <p className="modal-price">${price}</p>
                        <p className="modal-text">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusamus ea laudantium saepe praesentium asperiores error quidem dolor dolore numquam esse voluptatibus ipsam fuga veniam pariatur, suscipit laborum quia fugit non!</p>

                    </div>
                </div>
                <div className="modal-wrap-btn">

                    <Button
                        onClick={() => {
                            onCart({
                                'deleteCartArticle': article
                            })
                            onClose()

                        }}
                        className='modal-btn'
                    >
                        Delete card
                    </Button>

                </div>
            </div>

        </Modal>

    )
}

export default ModalBasket