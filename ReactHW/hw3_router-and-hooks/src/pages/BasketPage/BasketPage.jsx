import { useState } from 'react'
import Card from '../../components/CardGood/Card'
import Container from '../../components/layout/Container/Container'
import ModalBasket from './ModalBasket/ModalBasket.jsx'
import './BasketPage.scss'
import { Link } from "react-router-dom"




const BasketPage = (props) => {

    const { favorite, onFavorite, cart, onCart } = props

    const [isModal, setIsModal] = useState(false)

    const handleModalCard = () => {
        setIsModal(!isModal)
    }

    const [currentCard, setCurrentCard] = useState({})
    const handleCurrentCard = (card) => {
        setCurrentCard(card)
    }




    return (
        <section className="sect-basket">
            <Container>

                <h1 className="sect-title  sect-basket__title">My basket products </h1>
                <ul className="list-basket">
                    {cart.map((item, index) => {
                        return <Card
                            favorite={favorite}
                            key={index}
                            product={item}
                            showModalCard={handleModalCard}
                            handleCurrentCard={handleCurrentCard}
                            deleteCart='true'
                            cart={cart}
                            onFavorite={onFavorite}
                        />
                    })}
                </ul>
                {!cart.length && <div className="list-basket-empty">
                    <p className="list-basket-empty__text">
                        Oops! Items are missing, check out our <Link to='/limelight' >best items</Link>

                    </p>

                </div>}
            </Container>

            {isModal && <ModalBasket
                onClose={handleModalCard}
                currentCard={currentCard}
                onCart={onCart}
            />}
        </section>
    )
}


export default BasketPage