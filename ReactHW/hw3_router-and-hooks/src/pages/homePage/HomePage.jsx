
import Main from "../../components/Main/Main.jsx"

import { useEffect, useState } from 'react'



const HomePage = (props) => {
    const { favorite, cart, onFavorite, onCart } = props
    return (
        <Main
            cart={cart}
            favorite={favorite}
            onCart={onCart}
            onFavorite={onFavorite}
        />
    )
}

export default HomePage