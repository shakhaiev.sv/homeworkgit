import { Routes, Route } from "react-router-dom";

import Main from '../components/Main/Main.jsx'
import NotPage from '../pages/notPages/NotPage.jsx'
import FavoritePage from "../pages/FavoritePage/FavoritePage.jsx";
import BasketPage from '../pages/BasketPage/BasketPage.jsx'
import Limelight from "../components/Main/Limelight/Limelight.jsx";



export default (props) => {

    const { onFavorite, favorite, onCart, cart } = props

    return (
        <Routes>
            <Route path="/" element={
                <Main
                    cart={cart}
                    favorite={favorite}
                    onCart={onCart}
                    onFavorite={onFavorite}
                />}
            />
            <Route path='/favorite' element={
                <FavoritePage
                    favorite={favorite}
                    onFavorite={onFavorite}
                    onCart={onCart}
                    cart={cart}

                />}
            />
            <Route path='/basket' element={
                <BasketPage
                    favorite={favorite}
                    onFavorite={onFavorite}
                    onCart={onCart}
                    cart={cart}

                />}
            />
            <Route path='/limelight' element={
                <Limelight
                    favorite={favorite}
                    cart={cart}
                    onCart={onCart}
                    onFavorite={onFavorite}
                />}
            />
            <Route path="*" element={<NotPage />} />

        </Routes>
    )
}