import Button from "../../Button/Button"
import Container from "../../layout/Container/Container"
import BtnPrevious from './images/svg/BtnPrevious.svg?react'
import BtnNext from './images/svg/BtnNext.svg?react'
import "./InfoSection.scss"


const InfoSection = () => {
    return (
        <section className="sect-info">
            <Container>
                <div className="sect-info-wrap">
                    <Button
                        type='button'
                        className='btn-next'>
                        <BtnPrevious />
                    </Button>
                    <div className="sect-info-wrapper">
                        <p className="sect-info-subtitle">T-shirt / Tops</p>
                        <h1 className="sect-info-title">Summer <br /> Value Pack</h1>
                        <p className="sect-info-text">cool / colorful / comfy</p>
                        <Button
                            href='#'
                            className='sect-info__link-shop'
                        >
                            Shop Now
                        </Button>
                    </div>
                    <Button
                        type='button'
                        className='btn-prev'>
                        <BtnNext />
                    </Button>
                </div>
                <div className="sect-info-caresul">
                    <span className="active"></span>
                    <span></span>
                </div>
            </Container>
        </section >
    )
}

export default InfoSection