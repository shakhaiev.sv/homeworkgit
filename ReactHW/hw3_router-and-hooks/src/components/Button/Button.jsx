import './Button.scss'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { Link } from 'react-router-dom'


const Button = (props) => {

    const {
        type = 'button',
        className,
        children,
        onClick,
        to,
        ...restProps
    } = props


    let Element = to ? Link : 'button'


    return (
        <Element
            onClick={onClick}
            className={cn("btn", className)}
            type={!to && type}
            to={to}
            {...restProps}
        >
            {children}
        </Element>)
}


Button.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func
}

export default Button