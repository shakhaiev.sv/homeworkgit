import { useEffect, useState } from "react"
import { sendRequest } from "../../helpers/sendRequest"
import Card from "../CardGood/Card.jsx"
import cn from 'classnames'
import './ListCard.scss'
import PropTypes from 'prop-types'

const ListCard = (props) => {

    const { classUl,
        classLi,
        url,
        showModalCard,
        handleCurrentCard,
        onCart,
        onFavorite,
        favorite,
        cart } = props;


    const [listCard, setListCard] = useState([]);

    useEffect(() => {
        sendRequest(url)
            .then((data) => setListCard(data))
    }, [])

    return (
        <ul className={cn('list-card', classUl)}>
            {listCard.map((item, index) => {
                return < Card
                    key={index}
                    className={classLi}
                    product={item}
                    showModalCard={showModalCard}
                    handleCurrentCard={handleCurrentCard}
                    onCart={onCart}
                    onFavorite={onFavorite}
                    favorite={favorite}
                    cart={cart}
                />
            })
            }
        </ul >
    )
}

ListCard.propTypes = {
    classUl: PropTypes.string,
    classLi: PropTypes.string,
    url: PropTypes.string,
    showModalCard: PropTypes.func,
    handleCurrentCard: PropTypes.func,
    onCart: PropTypes.func,
    onFavorite: PropTypes.func,
    favorite: PropTypes.array,
    cart: PropTypes.array,

}

export default ListCard