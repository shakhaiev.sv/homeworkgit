import './ModalCard.scss'
import Modal from "../Modal/Modal.jsx"
import Button from "../Button/Button.jsx"
import FavoriteSvg from './images/svg/favorite.svg?react'
import PropTypes from 'prop-types'


const ModalCard = (props) => {
    const { onClose,
        currentCard,
        onCart,
        onFavorite,
        favorite,
        cart } = props

    const { urlImg, title, price, brand, article, item, availabilityColor, color } = currentCard

    const doubleFavorite = favorite?.some((item) => item.article === article)
    const doubleCart = cart?.some((item) => item.article === article)

    return (
        <Modal
            onClose={onClose}
            currentCard={currentCard}
        >
            <div className="modal-limelight">
                <div className="modal-info">
                    <div className='modal-img'>
                        <img src={urlImg} alt={title} />
                        {doubleFavorite && <div className='modal-img-favorite'><FavoriteSvg /></div>}
                    </div>
                    <div className="modal-desc">
                        <h1 className="modal-title">{title}</h1>
                        <p className="modal-brand">{brand}</p>
                        <p className="modal-price">${price}</p>
                        <p className="modal-text">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusamus ea laudantium saepe praesentium asperiores error quidem dolor dolore numquam esse voluptatibus ipsam fuga veniam pariatur, suscipit laborum quia fugit non!</p>

                    </div>
                </div>

                <div className="modal-wrap-btn">
                    <Button
                        onClick={() => {
                            if (cart) {
                                if (doubleCart) {
                                    // видалити картку 
                                    onCart({
                                        'deleteCartArticle': article
                                    })
                                    onClose()
                                } else {
                                    // додати картку 
                                    const btnCart = item.querySelector('.btn-goods[data-dbattrcart]')
                                    btnCart.dataset.dbattrcart = true
                                    onCart({
                                        'urlImg': urlImg,
                                        'title': title,
                                        "price": price,
                                        "brand": brand,
                                        'article': article,
                                        'availabilityColor': availabilityColor,
                                        'color': color,
                                        'dbattrcart': btnCart.dataset.dbattrcart
                                    })
                                    onClose()

                                }
                            }
                        }}
                        className='modal-btn'
                    >
                        Add to cart
                    </Button>
                    <Button
                        onClick={() => {
                            if (favorite) {
                                if (doubleFavorite) {

                                    // видалити картку 
                                    onFavorite({
                                        'deleteCartArticle': article
                                    })
                                    onClose()
                                } else {
                                    // додати картку 
                                    const btnFavorite = item.querySelector('.btn-goods[data-dbfavorite]')
                                    btnFavorite.dataset.dbfavorite = true
                                    onFavorite({
                                        'urlImg': urlImg,
                                        'title': title,
                                        "price": price,
                                        "brand": brand,
                                        'article': article,
                                        'availabilityColor': availabilityColor,
                                        'color': color,
                                        'dbfavorite': btnFavorite.dataset.dbfavorite
                                    })
                                    onClose()

                                }
                            }

                        }}
                        className='modal-btn'
                    >
                        Add to favorites
                    </Button>
                </div>
            </div>

        </Modal >
    )
}

ModalCard.propTypes = {
    onClose: PropTypes.func,
    onCart: PropTypes.func,
    onFavorite: PropTypes.func,
    currentCard: PropTypes.object,
    favorite: PropTypes.array,
    cart: PropTypes.array

}

export default ModalCard