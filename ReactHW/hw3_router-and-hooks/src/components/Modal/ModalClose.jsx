
import Button from "../Button/Button"
import CloseSvg from './images/svg/close.svg?react'
import PropTypes from 'prop-types'

export const ModalClose = (props) => {
    const { onClose } = props
    return (
        <Button
            type='button'
            className='btn-close'
            onClick={onClose}
        >
            <CloseSvg />
        </Button>

    )

}

ModalClose.propTypes = {
    onClose: PropTypes.func,

}