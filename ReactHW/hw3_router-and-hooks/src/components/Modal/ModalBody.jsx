import { Children } from "react";
import Button from "../Button/Button";



export const ModalBody = ({ children }) => {
    return (
        <>
            <div className="modal-content">
                {children}
            </div>
        </>

    )
} 