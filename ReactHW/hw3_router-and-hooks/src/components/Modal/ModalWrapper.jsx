import PropTypes from 'prop-types'

export const ModalWrapper = (props) => {
    const { children, onClose } = props
    return (
        <div className="modal-wrap" onClick={(e) => {
            if (e.target.classList.contains('modal-wrap')) {
                onClose()
            }
        }}>
            {children}
        </div>

    )
}

ModalWrapper.propTypes = {
    onClose: PropTypes.func,

}