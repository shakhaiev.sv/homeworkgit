let btnBurgmenu = document.querySelector('.hd-burger-menu');
let navMenu = document.body.querySelector('.hd-nav');

btnBurgmenu.addEventListener('click', function (e) {
    const btn = e.target.closest('.hd-burger-menu');
    if (btn) {
        const btnBurgorReset = btn.children[0];
        navMenu.classList.toggle('active');
        btnBurgorReset.classList.toggle('burg-line');
        btnBurgorReset.classList.toggle('burg-reset');
    }
});

window.addEventListener('resize', function () {
    if (window.innerWidth > 780) {
        btnBurgmenu.children[0].classList.replace('burg-reset', 'burg-line')
        navMenu.classList.remove('active');
    }
})