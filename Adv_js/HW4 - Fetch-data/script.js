// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// Відповідь : AJAX  -  дозволяє веб-сторінці взаємодіяти з сервером без необхідності перезавантажувати всю сторінку.
//            Перевага AJAX полягає в тому, що вона дозволяє здійснювати оновлення сторінки частково,
//            без повного перезавантаження, що робить взаємодію з веб-сторінкою швидшою та зручнішою для користувачів.

const API = "https://ajax.test-danit.com/api/swapi";
const listFilms = document.querySelector(".wrap");

function sendRequest(url, method = "GET", config) {
  return fetch(url, { method, ...config });
}

function renderFilms() {
  sendRequest(`${API}/films`)
    .then((res) => res.json())
    .then((data) => {
      const ul = document.createElement("ul");
      data
        .sort((a, b) => {
          return a.episodeId - b.episodeId;
        })
        .forEach(({ name, episodeId, openingCrawl, characters }) => {
          try {
            let li = document.createElement("li");
            let classItem = `ep-${episodeId}`;
            li.classList.add(classItem, "anim");
            li.innerHTML = `
                <h1>${name}</h1>
                <h4><b>Episodе</b>: ${episodeId}</h4>
                <p><b>Content</b>: ${openingCrawl}</p>
                `;
            ul.append(li);
            getPersonFilm(characters, classItem);
          } catch (e) {
            console.log(e);
          }
        });
      listFilms.append(ul);
    });
}
renderFilms();

function getPersonFilm(arrayLink, itemClass) {
  const arrayRequest = arrayLink.map((it) => fetch(it));

  Promise.all(arrayRequest)
    .then((responce) => {
      const result = responce.map((it) => it.json());
      const item = document.getElementsByClassName(itemClass)[0];
      if (responce) {
        item.classList.remove("anim");
      }

      Promise.all(result)
        .then((data) => {
          const listPerson = data.map((it) => it.name);
          let elPersonFilm = document.createElement("p");
          elPersonFilm.innerHTML = `
                    <b>Character list</b>: ${listPerson.join(", ")}
                    `;
          document.querySelector(`.${itemClass}`).append(elPersonFilm);
        })
        .catch((e) => console.log(e));
    })
    .catch((e) => console.log(e));
}
