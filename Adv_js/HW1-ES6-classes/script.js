// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Відповідь: Кожен об'єкт має свої методи, властивості і прототип. 
//            Якщо ми намагаємося отримати властивість або метод із об'єкта, і 
//            він не знаходить в самому обєкті, то JS  почне шукати його в 
//            прототипі, якщо в протопипі не знайде, то пошук буде 
//            продовжуватися глибше по ланцюжку прототипів, поки властивість ну буде знайдена 
//            або дійде до кінця  ланцюжка. 

// Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Відповідь: Виклик super() у конструкторі класу-нащадка  викликає конструктор батьківсього класу.
//            super() надає можливіть нащадку використовувати методи та властивості бaтьківського класу.



class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age
        this._salary = salary
    }

    set name(value) {
        this._name = value
    }
    get name() {
        return this._name
    }

    set age(value) {
        this._age = value
    }
    get age() {
        return this._age
    }

    set salary(value) {
        this._salary = value
    }
    get salary() {
        return this._salary
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang
    }

    get salary() {
        return this._salary * 3
    }
}

const listProgrammer = [
    new Programmer('John', 20, 20000, ["HTML", "CSS"]),
    new Programmer('Peter', 25, 40000, ["C++", "NodeJS"]),
    new Programmer('Drake', 30, 60000, ["HTML", "CSS", "JS", "C++", "C#"]),
]
console.log(listProgrammer);
console.log(listProgrammer[2].salary);


