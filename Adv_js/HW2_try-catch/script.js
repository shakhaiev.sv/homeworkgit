
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


function createListBooks(data) {
    try {
        const root = document.getElementById('root')
        const ul = document.createElement('ul')
        data.forEach((el) => {
            const status = isObjectproperty(el, 'name', 'author', 'price')
            if(!status.checker){
                console.log(new Error (`Відсутня властивість ${status.propertyNot}`));
            }else{
                const li = document.createElement('li')
                li.textContent = `Книга - "${el.name}" Автор - "${el.author}" Ціна - "${el.price}"`
                ul.append(li)  
            }
        })
        root.append(ul)
    } catch (e) {
        console.log(e.name + ' : ' + e.message);
    }
}


createListBooks(books)


function isObjectproperty (obj, ...arg){
    const newObj = {
        checker: true,
    }
    arg.forEach((el)=>{
        if(!obj.hasOwnProperty(el)){
            newObj.checker=false;
            newObj.propertyNot = el
        }
    })
    return newObj
}

