const API_USERS = "https://ajax.test-danit.com/api/json/users";
const API_POSTS = "https://ajax.test-danit.com/api/json/posts";

const API = [API_POSTS, API_USERS];
const elRoot = document.querySelector(".root");

function removeShowLoader() {
  document.querySelector(".wrap-loader").remove();
}

// Class

class Card {
  constructor(userId, postId, name, email, title, body) {
    this.userId = userId;
    this.postId = postId;
    this.name = name;
    this.email = email;
    this.title = title;
    this.body = body;
  }

  addCard() {
    const contentCard = this.createCard();
    elRoot.insertAdjacentHTML("afterbegin", contentCard);
  }
  createCard() {
    const textCard = `
      <div class='user-card' data-user="${this.userId}" data-post="${
      this.postId
    }">
        <div class='img'>${this.name[0].toUpperCase()}${this.name[
      this.name.indexOf(" ") + 1
    ].toUpperCase()} 
        </div>
        <div class='info'>
          <header>
            <p class="user-name">${this.name}</p>
            <p class="user-email">${this.email}</p>
          </header>
          <h5 class="user-title">${this.title}</h5>
          <p class="user-body">${this.body}</p>
          <div class='popular-card'>
            <div class='pop-item'>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 18 18" fill="none"><path fill-rule="evenodd" clip-rule="evenodd" d="M15.3204 3.3593C14.0595 1.49195 11.507 1.55555 10.3306 3.50742L9.85651 4.29405L8.14358 4.29405L7.66947 3.50743C6.49306 1.55555 3.94059 1.49195 2.6797 3.35931C1.63533 4.90601 1.81588 7.05644 3.07361 8.36913L9.00003 14.5546L14.9265 8.36913C16.1842 7.05643 16.3648 4.90601 15.3204 3.3593ZM9.00004 1.91963C6.93127 -0.72031 2.98032 -0.659869 1.02218 2.2401C-0.539051 4.55226 -0.292229 7.74709 1.62948 9.75279L9.00002 17.4454L16.3706 9.75278C18.2923 7.74709 18.5391 4.55226 16.9779 2.2401C15.0198 -0.659875 11.0688 -0.720315 9.00004 1.91963Z" fill="#ffffff"></path></svg>
            <span>3,456</span>
            </div>  
            <div class='pop-item'>
           <svg fill="white" height="24" stroke-width="1.5" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M3 20.2895V5C3 3.89543 3.89543 3 5 3H19C20.1046 3 21 3.89543 21 5V15C21 16.1046 20.1046 17 19 17H7.96125C7.35368 17 6.77906 17.2762 6.39951 17.7506L4.06852 20.6643C3.71421 21.1072 3 20.8567 3 20.2895Z" stroke="currentColor" stroke-width="1.5"/></svg>
           <span>1,456</span>
            </div>  
          </div>
        </div>
        <div class="btn-wrap">
        <button class='btn btn_edit'>
      <svg fill="none" height="20" viewBox="0 0 20 20" width="20" xmlns="http://www.w3.org/2000/svg"><path d="M14.4987 3C15.8793 3 16.9985 4.11916 16.9985 5.49972V9.00234C16.6571 9.0179 16.3179 9.0941 15.9986 9.23094V6.99956H3.99989V14.4987C3.99989 15.3271 4.67139 15.9986 5.49972 15.9986H9.47395C9.45833 16.0499 9.44398 16.1017 9.43092 16.1539L9.21979 16.9985H5.49972C4.11916 16.9985 3 15.8793 3 14.4987V5.49972C3 4.11916 4.11916 3 5.49972 3H14.4987ZM14.4987 3.99989H5.49972C4.67139 3.99989 3.99989 4.67139 3.99989 5.49972V5.99967H15.9986V5.49972C15.9986 4.67139 15.3271 3.99989 14.4987 3.99989ZM10.9789 15.3758L15.8078 10.5469C16.538 9.81666 17.722 9.81666 18.4523 10.5469C19.1826 11.2772 19.1826 12.4612 18.4523 13.1915L13.6234 18.0204C13.3419 18.3019 12.9891 18.5016 12.6028 18.5982L11.1051 18.9726C10.4537 19.1355 9.86376 18.5455 10.0266 17.8942L10.401 16.3964C10.4976 16.0101 10.6973 15.6574 10.9789 15.3758Z" fill="#ffffff"/></svg>
        </button>
        <button type="button" class="btn btn_del">
        <svg height="32" fill="white" version="1.1" viewBox="0 0 32 32" width="32" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Guides__x26__Forms"/><g id="Icons"><polygon points="21.657,8.929 16,14.586 10.343,8.929 8.929,10.343 14.586,16 8.929,21.657 10.343,23.071 16,17.414 21.657,23.071    23.071,21.657 17.414,16 23.071,10.343  "/></g></svg>
        </button>
        <div>
      </div>
      `;

    return textCard;
  }

  renderCard() {
    const contentCard = this.createCard();
    elRoot.insertAdjacentHTML("beforeend", contentCard);
  }
}

class Form {
  constructor(classForm, { input, textarea, btnClose, btnSubmit }) {
    this.classForm = classForm;
    this.input = input;
    this.textarea = textarea;
    this.btnClose = btnClose;
    this.btnSubmit = btnSubmit;
  }
  createFormGroup(array) {
    const formInputAll = [];
    array.forEach(({ tag, name, content }) => {
      let formInput = `
        <div class="form-group"> 
            <label class='label' for="${name}">${content}</label><br>
            <${tag} type="text" id="${name}" name="${name}">
        </div>
        `;
      formInputAll.push(formInput);
    });
    return formInputAll.join("");
  }

  createTextArea({ name, content }) {
    return `
                <div class="form-group">
                    <label   class='label' for="${name}">${content}</label><br>
                    <textarea name="${name}" id="${name}" cols="30" rows="10"></textarea>
                </div>
    `;
  }

  createBtn({ elClass, text, type }) {
    return `
    <button type="${type}" class="${elClass}">
    ${text}
    </button>
    `;
  }

  createInnerForm() {
    return `
        <form class="form-card ${this.classForm}">
        ${this.createBtn(this.btnClose)}
        ${this.createFormGroup(this.input)}
        ${this.createTextArea(this.textarea)}
        ${this.createBtn(this.btnSubmit)}
        </form>
    `;
  }

  createElWrap() {
    return `
    <div class="backdrop">
        <div class="modal">
            ${this.createInnerForm()}
        </div>
    </div>
    `;
  }

  renderForm() {
    document.body.insertAdjacentHTML("beforeend", this.createElWrap());
  }
}

const formAddCards = new Form("add", {
  input: [
    {
      tag: "input",
      name: "fname",
      content: "First name:",
    },
    {
      tag: "input",
      name: "lname",
      content: "Last name:",
    },
    {
      tag: "input",
      name: "email",
      content: "Email:",
    },
    {
      tag: "input",
      name: "title",
      content: "Title:",
    },
  ],
  textarea: {
    name: "body",
    content: "Body: ",
  },
  btnClose: {
    type: "button",
    elClass: "btn btn-close btn_close",
    text: ' <svg height="32" fill="white" version="1.1" viewBox="0 0 32 32" width="32" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Guides__x26__Forms"/><g id="Icons"><polygon points="21.657,8.929 16,14.586 10.343,8.929 8.929,10.343 14.586,16 8.929,21.657 10.343,23.071 16,17.414 21.657,23.071    23.071,21.657 17.414,16 23.071,10.343  "/></g></svg>',
  },
  btnSubmit: {
    type: "submit",
    elClass: "btn btn-primary",
    text: "Submit",
  },
});

const formUpdateCards = new Form("update", {
  input: [
    {
      tag: "input",
      name: "fname",
      content: "First name:",
    },
    {
      tag: "input",
      name: "lname",
      content: "Last name:",
    },
    {
      tag: "input",
      name: "email",
      content: "Email:",
    },
    {
      tag: "input",
      name: "title",
      content: "Title:",
    },
  ],
  textarea: {
    name: "body",
    content: "Body: ",
  },
  btnClose: {
    type: "button",
    elClass: "btn btn-close btn_close",
    text: ' <svg height="32" fill="white" version="1.1" viewBox="0 0 32 32" width="32" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Guides__x26__Forms"/><g id="Icons"><polygon points="21.657,8.929 16,14.586 10.343,8.929 8.929,10.343 14.586,16 8.929,21.657 10.343,23.071 16,17.414 21.657,23.071    23.071,21.657 17.414,16 23.071,10.343  "/></g></svg>',
  },
  btnSubmit: {
    type: "submit",
    elClass: "btn btn-primary",
    text: "Update",
  },
});

// function request
function sendRequest(url, method = "GET", config) {
  return fetch(url, { method, ...config });
}

function postCard(url, data) {
  return sendRequest(url, "POST", {
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
    },
  });
}

function deleteCard(id) {
  return sendRequest(`${API_POSTS}/${id}`, "DELETE");
}

function putCard(url, data) {
  return sendRequest(url, "PUT", {
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
    },
  });
}

function getAllRequest(arrayUrl, callback) {
  const arrayRequest = arrayUrl.map((item) => fetch(item));
  Promise.all(arrayRequest).then((res) => {
    const result = res.map((item) => item.json());
    Promise.all(result).then(([posts, users]) => {
      posts.forEach((item) => {
        const { id: postId, userId, title, body } = item;
        const user = users.find((it) => it.id === userId);
        const { name, email } = user;
        let card = new Card(userId, postId, name, email, title, body);
        card.renderCard();
      });
    });
  });
  callback();
}

getAllRequest(API, removeShowLoader);

// func content

function addNewCardBtn() {
  let btnAddCard = ` 
                        <button type="button" class="btn btn-fixed btn_add">
                        <svg height="48" viewBox="0 0 48 48" width="48" xmlns="http://www.w3.org/2000/svg"><path d="M0 0h48v48H0z" fill="#ffffff"/><path d="M28 20H4v4h24v-4zm0-8H4v4h24v-4zm8 16v-8h-4v8h-8v4h8v8h4v-8h8v-4h-8zM4 32h16v-4H4v4z"/></svg>
                        </button>
                        `;
  elRoot.insertAdjacentHTML("afterbegin", btnAddCard);
}
addNewCardBtn();
// func event
function closeFormBtn(form) {
  form.addEventListener("click", (e) => {
    if (!e.target.closest(".btn_close")) return;
    const elWrapForm = e.target.closest(".backdrop");
    elWrapForm.classList.remove("active");
    elWrapForm.remove();
  });
}

function submitNewCard(form) {
  form.addEventListener("submit", (e) => {
    e.preventDefault();

    const formFname = e.target.querySelector("#fname").value;
    const formLname = e.target.querySelector("#lname").value;
    const fullName = formFname + " " + formLname;
    const formEmail = e.target.querySelector("#email").value;
    const formTitle = e.target.querySelector("#title").value;
    const formBody = e.target.querySelector("#body").value;

    Promise.all([
      postCard(API_USERS, {
        name: fullName,
        email: formEmail,
      }),
      postCard(API_POSTS, {
        title: formTitle,
        body: formBody,
      }),
    ]).then((res) => {
      const result = res.map((it) => it.json());
      Promise.all(result).then(([user, post]) => {
        const { id: userId, name, email } = user;
        const { id: postId, title, body } = post;
        const newPostAdd = new Card(userId, postId, name, email, title, body);
        newPostAdd.addCard();
      });
    });

    const elWrapForm = e.target.closest(".backdrop");
    elWrapForm.classList.remove("active");
    elWrapForm.remove();
  });
}

document.querySelector(".btn_add").addEventListener("click", (e) => {
  if (!e.target.closest(".btn_add")) return;
  formAddCards.renderForm();
  document.querySelector(".backdrop").classList.add("active");
  const form = document.querySelector(".form-card.add");
  closeFormBtn(form);
  submitNewCard(form);
});

function submitUpdateCard(
  form,
  { userId, postId, userFname, userLname, userEmail, postTitle, postBody }
) {
  form.querySelector("#fname").value = userFname;
  form.querySelector("#lname").value = userLname;
  form.querySelector("#email").value = userEmail;
  form.querySelector("#title").value = postTitle;
  form.querySelector("#body").value = postBody;

  form.addEventListener("submit", (e) => {
    e.preventDefault();
    const user = {};
    const post = {};

    const formFname = e.target.querySelector("#fname").value;
    const formLname = e.target.querySelector("#lname").value;
    const formEmail = e.target.querySelector("#email").value;
    const formTitle = e.target.querySelector("#title").value;
    const formBody = e.target.querySelector("#body").value;

    if (formFname !== userFname || formLname !== userLname) {
      user.name = formFname + " " + formLname;
    }
    if (formEmail !== userEmail) user.email = formEmail;
    if (formTitle !== postTitle) post.title = formTitle;
    if (formBody !== postBody) post.body = formBody;

    if (Object.keys(user).length && !Object.keys(post).length) {
      putCard(`${API_USERS}/${userId}`, user)
        .then((res) => res.json())
        .then(({ name, email }) => {
          const el = document.querySelector(`[data-post="${postId}"]`);
          el.querySelector(".user-name").textContent =
            name ?? formFname + " " + formLname;
          el.querySelector(".user-email").textContent = email ?? formEmail;
        });
    } else if (!Object.keys(user).length && Object.keys(post).length) {
      putCard(`${API_POSTS}/${postId}`, post)
        .then((res) => res.json())
        .then(({ title, body }) => {
          const el = document.querySelector(`[data-post="${postId}"]`);
          el.querySelector(".user-title").textContent = title ?? formTitle;
          el.querySelector(".user-body").textContent = body ?? formBody;
        });
    } else if (Object.keys(user).length && Object.keys(post).length) {
      Promise.all([
        putCard(`${API_USERS}/${userId}`, user),
        putCard(`${API_POSTS}/${postId}`, post),
      ]).then((res) => {
        const result = res.map((item) => item.json());
        Promise.all(result).then(([user, post]) => {
          const { name, email } = user;
          const { title, body } = post;
          const el = document.querySelector(`[data-post="${postId}"]`);
          el.querySelector(".user-name").textContent =
            name ?? formFname + " " + formLname;
          el.querySelector(".user-email").textContent = email ?? formEmail;
          el.querySelector(".user-title").textContent = title ?? formTitle;
          el.querySelector(".user-body").textContent = body ?? formBody;
        });
      });
    } else {
      alert("the data has not been changed");
    }

    const elWrapForm = e.target.closest(".backdrop");
    elWrapForm.classList.remove("active");
    elWrapForm.remove();
  });
}

elRoot.addEventListener("click", (e) => {
  if (e.target.closest(".btn_del")) {
    const elCard = e.target.closest(".user-card");
    const cardIdPost = elCard.dataset.post;
    if (cardIdPost == 101) {
      elCard.remove();
      console.log(`Delete card ID ${cardIdPost}`);
    } else {
      deleteCard(cardIdPost).then((res) => {
        if (res.ok) {
          console.log(`Delete card ID ${cardIdPost}`);
          elCard.remove();
        }
      });
    }
  }
  if (e.target.closest(".btn_edit")) {
    formUpdateCards.renderForm();
    document.querySelector(".backdrop").classList.add("active");
    const form = document.querySelector(".form-card.update");
    closeFormBtn(form);

    const el = e.target.closest(".user-card");
    const userId = el.dataset.user;
    const postId = el.dataset.post;
    const [userFname, userLname] = el
      .querySelector(".user-name")
      .textContent.split(" ");
    const userEmail = el.querySelector(".user-email").textContent;
    const postTitle = el.querySelector(".user-title").textContent;
    const postBody = el.querySelector(".user-body").textContent;
    submitUpdateCard(form, {
      userId,
      postId,
      userFname,
      userLname,
      userEmail,
      postTitle,
      postBody,
    });
  }
});
