// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
// Відповідь : Можливість виконувати код не блокуючи основний потік

const API_IP = "https://api.ipify.org/?format=json";
const API_IP_LOCATION = "http://ip-api.com/";
const blockRoot = document.querySelector("#root");
const btn = document.querySelector(".btn_ip");

async function sendRequest(url, method = "GET", config) {
  const responce = await fetch(url, { method, ...config });
  return responce;
}

async function getRequestIp() {
  const res = await sendRequest(API_IP);
  const { ip } = await res.json();
  const responceIP = await sendRequest(`${API_IP_LOCATION}json/${ip}`);
  const { timezone, country, city, regionName } = await responceIP.json();
  blockRoot.insertAdjacentHTML(
    "beforeend",
    `
      <div>Континент - ${timezone}</div>
      <div>Країна- ${country}</div>
      <div>Регіон- ${regionName}</div>
    <div>Місто - ${city}</div>
    `
  );
}

btn.addEventListener("click", (e) => {
  if (e.detail === 1) {
    getRequestIp();
  } else {
    e.preventDefault();
  }
});
